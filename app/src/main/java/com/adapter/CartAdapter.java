/*
package com.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.foodapp.CartScreen;
import com.foodapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

*/
/**
 * Created by firemoon on 4/5/17.
 *//*


public class CartAdapter extends BaseAdapter {

    private List<JSONObject> originalData = new ArrayList<>();
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    int layout;
    Activity ctx;


    public CartAdapter(Activity context, ArrayList<JSONObject> objectDatabase){
//        layout=order_item_recycle;
        ctx=context;
        originalData=objectDatabase;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return originalData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return originalData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        TextView tv1, tv2, tv3,tv4,tv5;
        LayoutInflater inflater =ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.cart_design, parent, false);

        try {

            tv1= (TextView) row1.findViewById(R.id.item_title);
            tv2= (TextView) row1.findViewById(R.id.qty_data);
            tv3= (TextView) row1.findViewById(R.id.price_item);
            tv4= (TextView) row1.findViewById(R.id.remove_order);
            tv5= (TextView) row1.findViewById(R.id.extracheeseprice);

            tv1.setText(originalData.get(position).getString("item_id"));
            tv2.setText(originalData.get(position).getString("quantity"));
            tv3.setText("USD "+originalData.get(position).getString("price"));

            tv4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CartScreen rrefence=new CartScreen();
                    try {
                        rrefence.removeCart(originalData.get(position).getString("cart_id"),ctx);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }
}
*/
