package com.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.constants.Utils;
import com.villasfood.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by firemoon on 2/5/17.
 */

public class CustomListAdapter extends BaseAdapter {

    private List<JSONObject> originalData = new ArrayList<>();
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    int layout;
    Activity ctx;


    public CustomListAdapter(Activity context, ArrayList<JSONObject> objectDatabase){
//        layout=order_item_recycle;
        ctx=context;
        originalData=objectDatabase;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return originalData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return originalData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        TextView tv1, tv2, tv3,tv4,tv5;
        de.hdodenhof.circleimageview.CircleImageView food_image = null;

        LayoutInflater inflater =ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.order_item_recycle, parent, false);
        tv1 = (TextView) row1.findViewById(R.id.recipie_name);
        tv2 = (TextView) row1.findViewById(R.id.recipie_price);
        tv3 = (TextView) row1.findViewById(R.id.date_order);
        tv4 = (TextView) row1.findViewById(R.id.time_order);
        tv5 = (TextView) row1.findViewById(R.id.order_place);
        food_image = (CircleImageView) row1.findViewById(R.id.fooddp);

        try {

            Utils.write("url  "+originalData.get(position).getString("item_img"));

            tv1.setText(originalData.get(position).getString("item_title"));
            tv2.setText("RP "+originalData.get(position).getString("price"));
            tv3.setText(originalData.get(position).getString("order_date"));
            tv4.setText(originalData.get(position).getString("time_on"));
            tv5.setText(originalData.get(position).getString("address"));


            UrlImageViewHelper.setUrlDrawable(food_image, Utils.encodeURL(originalData.get(position).getString("item_img")),
                    R.mipmap.recplaceholder, new UrlImageViewCallback() {
                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {

                            if (!loadedFromCache) {
                                ScaleAnimation scale = new ScaleAnimation(0, 1,
                                        0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f);
                                scale.setDuration(300);
                                scale.setInterpolator(new OvershootInterpolator());
                                imageView.startAnimation(scale);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }

}
