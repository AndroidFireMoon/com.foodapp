package com.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.villasfood.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by firemoon on 3/5/17.
 */

public class SeperationAdapter extends BaseAdapter {

    private List<JSONObject> originalData = new ArrayList<>();
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    int layout;
    HashMap<String, ArrayList<JSONObject>> checkHashMap;
    ArrayList<String> sortedString;
    Activity ctx;
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;


    public SeperationAdapter(Activity context, HashMap<String, ArrayList<JSONObject>> checkHashMap, ArrayList<String> sortedString, AsyncTaskJSONObject.AsynctaskListner callBackListner) {
        ctx=context;
        this.checkHashMap=checkHashMap;
        this.sortedString=sortedString;
        this.callBackListner=callBackListner;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return sortedString.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return sortedString.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ImageView imageUser;
        TextView name_user_order, tv3,tv4,dateTime;
        Button delete_order;
        LinearLayout bgColor;
        LayoutInflater inflater =ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.order_seperate, parent, false);
        imageUser = (ImageView) row1.findViewById(R.id.userprofilepic);
        name_user_order = (TextView) row1.findViewById(R.id.name_user_order);
        dateTime= (TextView) row1.findViewById(R.id.dateTime);
        delete_order = (Button) row1.findViewById(R.id.delete_order);
        bgColor = (LinearLayout) row1.findViewById(R.id.bgColor);

        try {
            if(position%2==0){
                bgColor.setBackgroundColor(Color.parseColor("#FFE3B1"));
            }else{
                bgColor.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }

            name_user_order.setText("ORDER "+sortedString.get(position));

            System.out.println("hashmap     "+checkHashMap.get(sortedString.get(position)));


            dateTime.setText("Date :"+checkHashMap.get(sortedString.get(position)).get(0).getString("order_on")+"\n"+ "Time :"+checkHashMap.get(sortedString.get(position)).get(0).getString("time_int"));



            delete_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<JSONObject> orderId=checkHashMap.get(sortedString.get(position));
                    ArrayList<String> cart_ids=new ArrayList<String>();
                    for(int i=0; i<orderId.size(); i++){
                        try {

                            Utils.write(" delete  cart id    "+orderId.get(i).getString("cart_id"));

                            cart_ids.add(orderId.get(i).getString("cart_id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    String  cart_id_concat=android.text.TextUtils.join("-", cart_ids);
                    deleteOrder(CommonUtils.getPreferencesString(ctx,"user_list_id"),cart_id_concat);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }

    public void deleteOrder(String user_id,String cart_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id",user_id);
        params.put("cart_id",cart_id);
        Utils.write(user_id);
        Utils.write(cart_id);
        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.SELECTED_ORDER);
    }
}
