package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.villasfood.AddEditCategoryCheff;
import com.villasfood.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by firemoon on 3/5/17.
 */

public class CategoryListCheffAdapter extends BaseAdapter {
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    private List<String> originalData = null;
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    int layout;
    Activity ctx;
    ArrayList<JSONObject> categoryData=new ArrayList<>();
    public CategoryListCheffAdapter(Activity context, ArrayList<JSONObject> categoryData, AsyncTaskJSONObject.AsynctaskListner callBackListner){
//        layout=order_item_recycle;
        ctx=context;
        this.categoryData=categoryData;
        this.callBackListner=callBackListner;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categoryData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return categoryData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        TextView food_name, tv2, tv3,tv4;
        ImageView food_image;
        LinearLayout editCategory,deleteCategory;
        LayoutInflater inflater =ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.categories, parent, false);
        food_name= (TextView) row1.findViewById(R.id.food_name);
        editCategory= (LinearLayout) row1.findViewById(R.id.editCategory);

        deleteCategory= (LinearLayout) row1.findViewById(R.id.deleteCategory);
        food_image= (ImageView) row1.findViewById(R.id.food_image);


        LinearLayout category_root= (LinearLayout) row1.findViewById(R.id.category_root);
       // category_root.setBackgroundColor(Color.parseColor("#3f4392"));


        try {

            editCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        CommonUtils.savePreferencesString(ctx,"name",categoryData.get(position).getString("cat_title"));
                        CommonUtils.savePreferencesString(ctx,"img",Utils.encodeURL(categoryData.get(position).getString("cat_img")));
                        ctx.startActivity(new Intent(ctx,AddEditCategoryCheff.class).putExtra("heading","Edit Category").putExtra("cat_img",categoryData.get(position).getString("cat_img")).putExtra("cat_title",categoryData.get(position).getString("cat_title")).putExtra("cat_id",categoryData.get(position).getString("cat_id")));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            deleteCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                      Utils.write("cat_id  "+categoryData.get(position).getString("cat_id"));
                      makeVolley(categoryData.get(position).getString("cat_id"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


           // food_name.setTextColor(Color.parseColor("#ffffff"));
            editCategory.setVisibility(View.VISIBLE);
            deleteCategory.setVisibility(View.VISIBLE);

            food_name.setText(categoryData.get(position).getString("cat_title"));


            UrlImageViewHelper.setUrlDrawable(food_image,Utils.encodeURL(categoryData.get(position).getString("cat_img")),
                    R.mipmap.recplaceholder, new UrlImageViewCallback() {
                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {

                            if (!loadedFromCache) {
                                ScaleAnimation scale = new ScaleAnimation(0, 1,
                                        0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f);
                                scale.setDuration(300);
                                scale.setInterpolator(new OvershootInterpolator());
                                imageView.startAnimation(scale);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }
    protected void makeVolley(String cat_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("cat_id",cat_id);
        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.DELETE_CAT);
    }



}
