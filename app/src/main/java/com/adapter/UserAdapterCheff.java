package com.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.villasfood.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by firemoon on 3/5/17.
 */

public class UserAdapterCheff extends BaseAdapter implements AsyncTaskJSONObject.AsynctaskListner{


    private List<JSONObject> originalData = new ArrayList<>();
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    int layout;
    Activity ctx;
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;


    public UserAdapterCheff(Activity context, ArrayList<JSONObject> objectDatabase, AsyncTaskJSONObject.AsynctaskListner callBackListner){
//        layout=order_item_recycle;
        ctx=context;
        originalData=objectDatabase;
        this.callBackListner=callBackListner;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return originalData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return originalData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ImageView imageUser;
        TextView name_user_order, tv3,tv4;
        Button save_paid_bills,delete_user;
        LinearLayout bgColor;
        LayoutInflater inflater =ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.order_by_cheff, parent, false);
        imageUser = (ImageView) row1.findViewById(R.id.userprofilepic);
        name_user_order = (TextView) row1.findViewById(R.id.name_user_order);
        bgColor = (LinearLayout) row1.findViewById(R.id.bgColor);
        save_paid_bills = (Button) row1.findViewById(R.id.save_paid_bills);
        delete_user = (Button) row1.findViewById(R.id.delete_user);


        try {
            if(position%2==0){
                bgColor.setBackgroundColor(Color.parseColor("#FFE3B1"));
            }else{
               bgColor.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            name_user_order.setText(originalData.get(position).getString("username"));

            save_paid_bills.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  emailUser();
                    try {
                        CommonUtils.savePreferencesString(ctx,"customer_id",originalData.get(position).getString("user_id"));

                        userListOrder(originalData.get(position).getString("user_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            delete_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        deleteUser(originalData.get(position).getString("user_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


            if (originalData.get(position).getString("user_img").length() > 0) {


                UrlImageViewHelper.setUrlDrawable(imageUser, Utils.encodeURL(originalData.get(position).getString("user_img")),
                        0, new UrlImageViewCallback() {
                            @Override
                            public void onLoaded(ImageView imageView,
                                                 Bitmap loadedBitmap, String url,
                                                 boolean loadedFromCache) {

                                if (!loadedFromCache) {
                                    ScaleAnimation scale = new ScaleAnimation(0, 1,
                                            0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f);
                                    scale.setDuration(300);
                                    scale.setInterpolator(new OvershootInterpolator());
                                    imageView.startAnimation(scale);
                                }
                            }
                        });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }


    public void userListOrder(String user_id){

        Utils.write("print             "+user_id);


        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id", user_id);
        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.SEPERATE_ORDER);
    }




    public void emailUser(String user_local_id, String total_order, String total_amount, ArrayList<Double> referList){

        String billingPrice=android.text.TextUtils.join(",", referList);

        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id",CommonUtils.getPreferencesString(ctx,"customer_id"));
        params.put("total_amount",total_amount);
        params.put("total_order",total_order);
        params.put("bill_amount",billingPrice);


        Utils.write("id      "+CommonUtils.getPreferencesString(ctx,"customer_id"));
        Utils.write("id      "+total_amount);
        Utils.write("id      "+total_order);
        Utils.write("id      "+billingPrice);

        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.PAID_BILLS);
    }

    public void deleteUser(String user_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id",user_id);
        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.DELETE_USER);
    }

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{
            if(action.equalsIgnoreCase(AppConstant.PAID_BILLS)){
                System.out.println("entry 1");
            }else{
                System.out.println("entry 2");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
