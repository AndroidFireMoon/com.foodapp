package com.adapter;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.villasfood.R;


import java.util.Random;

public class SamplePagerAdapter extends PagerAdapter {

    private final Random random = new Random();
    private int mSize;
    Activity context;
    Toolbar mToolbar;

    public SamplePagerAdapter(Activity ctx, Toolbar mToolbar) {
        mSize = 5;
        context=ctx;
        this.mToolbar=mToolbar;
    }

    public SamplePagerAdapter(int count) {
        mSize = count;
    }

    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {



        TextView textView = new TextView(view.getContext());
        textView.setText("Menu Item  "+String.valueOf(position + 1));
        textView.setBackgroundColor(0xff000000 | random.nextInt(0x00ffffff));
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(48);



        textView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //this will log the page number that was click
//               setNextActiveFragment(MenuFragment.newInstance(),true);
           //     context.startActivity(new Intent(context, OrderDetailUserFragment.class));



            }
        });

        view.addView(textView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return textView;
    }



    // pager adapter

    public void addItem() {
        mSize++;
        notifyDataSetChanged();
    }

    public void removeItem() {
        mSize--;
        mSize = mSize < 0 ? 0 : mSize;
        notifyDataSetChanged();
    }


    public void setNextActiveFragment(Fragment fragment, boolean addToBackStack) {


        if (addToBackStack) {
            context.getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.animation_leave_to_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.cotainer_frame, fragment)
                    .addToBackStack(null)
                    .commit();
        } else {
            context.getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.cotainer_frame, fragment)
                    .commit();
        }
    }

}