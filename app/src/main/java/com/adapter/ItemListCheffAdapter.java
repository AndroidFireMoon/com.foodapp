package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.villasfood.AddEditCategoryItem;
import com.villasfood.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by firemoon on 3/5/17.
 */

public class ItemListCheffAdapter extends BaseAdapter {
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    private List<JSONObject> originalData = new ArrayList<>();
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    int layout;
    Activity ctx;


    public ItemListCheffAdapter(Activity context, ArrayList<JSONObject> categoryData, AsyncTaskJSONObject.AsynctaskListner callBackListner){
//        layout=order_item_recycle;
        ctx=context;
        originalData=categoryData;
        this.callBackListner= callBackListner;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return originalData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return originalData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        TextView food_name, item_price, item_name,tv4;
        LinearLayout editCategory,deleteItem;
        de.hdodenhof.circleimageview.CircleImageView circle_item;

        LayoutInflater inflater =ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.add_item_list, parent, false);
        food_name= (TextView) row1.findViewById(R.id.food_name);
        item_name= (TextView) row1.findViewById(R.id.item_name);

        item_price= (TextView) row1.findViewById(R.id.item_price);
        editCategory= (LinearLayout) row1.findViewById(R.id.editCategory);
        deleteItem= (LinearLayout) row1.findViewById(R.id.deleteItem);
        circle_item= (CircleImageView) row1.findViewById(R.id.food_image);

        LinearLayout category_root= (LinearLayout) row1.findViewById(R.id.category_root);
      //  category_root.setBackgroundColor(Color.parseColor("#3f4392"));
        try {
          //  food_name.setTextColor(Color.parseColor("#ffffff"));
            editCategory.setVisibility(View.VISIBLE);
            item_price.setText("RP "+originalData.get(position).getString("item_price"));
            food_name.setText(originalData.get(position).getString("item_title"));
            item_name.setText(originalData.get(position).getString("item_title"));
            UrlImageViewHelper.setUrlDrawable(circle_item, Utils.encodeURL(originalData.get(position).getString("item_img")),
                    R.mipmap.circleplaceholder, new UrlImageViewCallback() {
                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {

                            if (!loadedFromCache) {
                                ScaleAnimation scale = new ScaleAnimation(0, 1,
                                        0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f);
                                scale.setDuration(300);
                                scale.setInterpolator(new OvershootInterpolator());
                                imageView.startAnimation(scale);
                            }

                        }
                    });

            Utils.write("item_id    "+originalData.get(position).getString("item_id"));

            editCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        CommonUtils.savePreferencesString(ctx,"item_id",originalData.get(position).getString("item_id"));
                        CommonUtils.savePreferencesString(ctx,"item_title",originalData.get(position).getString("item_title"));
                        CommonUtils.savePreferencesString(ctx,"item_img",originalData.get(position).getString("item_img"));
                        CommonUtils.savePreferencesString(ctx,"item_price",originalData.get(position).getString("item_price"));
                        ctx.startActivity(new Intent(ctx, AddEditCategoryItem.class).putExtra("heading","Edit Item"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            deleteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                         makeVolley(originalData.get(position).getString("item_id"));
                        } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }

    protected void makeVolley(String cat_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("item_id",cat_id);
        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.DELETE_ITM);
    }

}
