package com.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.CustomDialogClass;
import com.constants.Utils;
import com.villasfood.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by firemoon on 3/5/17.
 */

public class AdapterFoodCheffDetail extends BaseAdapter implements AsyncTaskJSONObject.AsynctaskListner {

    private List<JSONObject> originalData = new ArrayList<>();
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    int layout;
    Activity ctx;

    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    public AdapterFoodCheffDetail(Activity context, ArrayList<JSONObject> objectDatabase){
//        layout=order_item_recycle;
        ctx=context;
        originalData=objectDatabase;
        callBackListner=this;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return originalData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return originalData.get(position);
    }


    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        TextView tv1, tv2, tv3,tv4,tv5,tv6,commentUser;
        final Button statusFood;
        de.hdodenhof.circleimageview.CircleImageView food_image = null;
        LayoutInflater inflater =ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.order_item_recycle, parent, false);
        tv1 = (TextView) row1.findViewById(R.id.recipie_name);
        tv2 = (TextView) row1.findViewById(R.id.recipie_price);
        tv3 = (TextView) row1.findViewById(R.id.date_order);
        tv4 = (TextView) row1.findViewById(R.id.time_order);
        tv5 = (TextView) row1.findViewById(R.id.order_place);
        tv6 = (TextView) row1.findViewById(R.id.qty);
        commentUser = (TextView) row1.findViewById(R.id.commentUser);
        statusFood = (Button) row1.findViewById(R.id.statusFood);
        food_image = (CircleImageView) row1.findViewById(R.id.fooddp);

        try {
            tv1.setText(originalData.get(position).getString("item_title"));
            tv2.setText("RP "+originalData.get(position).getString("price"));
//            tv3.setText("Order Time "+originalData.get(position).getString("order_date"));
           // tv4.setText(originalData.get(position).getString("time_on"));
            tv5.setText(originalData.get(position).getString("address"));
            tv6.setText("Qty "+originalData.get(position).getString("quantity"));

            if(originalData.get(position).getString("time_int").equalsIgnoreCase("")){
                tv3.setVisibility(View.GONE);
            }else {
                tv3.setVisibility(View.VISIBLE);
                tv3.setText("Order Time - " + originalData.get(position).getString("time_int"));
            }

            if(originalData.get(position).getString("comment").equalsIgnoreCase("")){
                commentUser.setVisibility(View.GONE);
            }else {
                commentUser.setVisibility(View.VISIBLE);
                commentUser.setText("Order Comment - " + originalData.get(position).getString("comment"));
            }


            if(originalData.get(position).getString("order_status").equalsIgnoreCase("1"))
                statusFood.setText("DELIVERED");

           // commentUser.setText();

/*            billingFood.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   try {
                                                       getBilling(originalData.get(position).getString("price"),originalData.get(position).getString("order_date"),originalData.get(position).getString("order_id"));
                                                   } catch (JSONException e) {
                                                       e.printStackTrace();
                                                   }
                                               }
                                           }
            );*/


            statusFood.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   try {
                                                       setStatus(originalData.get(position).getString("order_id"),statusFood);
                                                   } catch (JSONException e) {
                                                       e.printStackTrace();
                                                   }
                                               }
                                           }
            );

            UrlImageViewHelper.setUrlDrawable(food_image, Utils.encodeURL(originalData.get(position).getString("item_img")),
                    R.mipmap.recplaceholder, new UrlImageViewCallback() {
                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {
                            if (!loadedFromCache) {
                                ScaleAnimation scale = new ScaleAnimation(0, 1,
                                        0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f);
                                scale.setDuration(300);
                                scale.setInterpolator(new OvershootInterpolator());
                                imageView.startAnimation(scale);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;

    }


    protected void exitFromApp() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ctx);

        builder.setMessage("Do you want to exit from user section?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int id) {
                     /*   Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                        context.sendBroadcast(broadcastIntent);
                        context.finishAffinity();*/

                        ctx.finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }


    TextView textView;
    protected void setStatus(final String order_id, final Button statusFood) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx)
                .setAdapter(
                        new ArrayAdapter<String>(ctx,
                                android.R.layout.simple_list_item_1,
                                new String[]{"Delievered"}) {
                            public View getView(int position, View convertView,
                                                ViewGroup parent) {
                                View view = super.getView(position,
                                        convertView, parent);
                                 textView = (TextView) view.findViewById(android.R.id.text1);

                                 textView.setTextColor(Color.BLACK);

                                return view;
                            }
                        }, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                statusFood.setText("Delivered");
                                setStatusService(order_id);
                            }
                        });

             builder.create().show();
    }//class ends


    protected void getBilling(String price, String order_date, String order_id) {
        CustomDialogClass cdd = new CustomDialogClass(ctx,price,order_date,order_id);
        cdd.show();
    } //  custom dialog
     //   //  // // //

    public void setStatusService(String order_id){
        Utils.write("order_id       "+order_id);
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("order_id", order_id);
        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.ORDER_STATUS);
    }



    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{


        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

