/*
package com.adapter;

*/
/**
 * Created by firemoon on 10/5/17.
 *//*


import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.constants.Utils;
import com.foodapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.foodapp.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

*/
/**
 * Created by firemoon on 10/5/17.
 *//*



public class MenuCorouselAdapter extends BaseAdapter implements Filterable {
     String stringVal;
    public static int counter=0;
    private ItemFilter mFilter = new ItemFilter();
    private List<String> originalData = null;
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    Activity ctx;
    ArrayList<JSONObject> datalist,plantData;
    ArrayList<String> subplantData;
    public MenuCorouselAdapter(Activity context, ArrayList<JSONObject> datalist){
        ctx=context;
        this.datalist=datalist;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return datalist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final TextView tv1, tv2, tv3;
        de.hdodenhof.circleimageview.CircleImageView food_icon;
        ImageView decrease,increase;
        LayoutInflater inflater = ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.menu_item_recycle, parent, false);
        tv1 = (TextView) row1.findViewById(R.id.recipie_name);
        tv2 = (TextView) row1.findViewById(R.id.recipie_price);
        tv3 = (TextView) row1.findViewById(R.id.quantity);
        decrease = (ImageView) row1.findViewById(R.id.decrease);
        increase = (ImageView) row1.findViewById(R.id.increase);
        food_icon = (de.hdodenhof.circleimageview.CircleImageView) row1.findViewById(R.id.food_icon);



        try {
            tv1.setText(datalist.get(position).getString("item_title"));
            tv2.setText("SGD  "+datalist.get(position).getString("item_price"));


            decrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(counter>1){
                        counter = Integer.parseInt(tv3.getText().toString());
                        counter-=1;
                        stringVal = Integer.toString(counter);
                        tv3.setText(stringVal);

                        try {
                            int money=Integer.parseInt(datalist.get(position).getString("item_price"));
                            int netWorth = money * counter;
                            tv2.setText("USD "+netWorth);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });

            increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        counter = Integer.parseInt(tv3.getText().toString());
                        counter+=1;
                        stringVal = Integer.toString(counter);
                        tv3.setText(stringVal);

                    try {
                        int money=Integer.parseInt(datalist.get(position).getString("item_price"));
                        int netWorth = money * counter;
                        tv2.setText("USD "+netWorth);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });




            UrlImageViewHelper.setUrlDrawable(food_icon, Utils.encodeURL(datalist.get(position).getString("item_img")),
                    R.mipmap.foodplaceholder, new UrlImageViewCallback() {
                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {

                            if (!loadedFromCache) {
                                ScaleAnimation scale = new ScaleAnimation(0, 1,
                                        0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f);
                                scale.setDuration(300);
                                scale.setInterpolator(new OvershootInterpolator());
                                imageView.startAnimation(scale);
                            }
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<String> list = subplantData;

            int count = list.size();
            final ArrayList<String> nlist = new ArrayList<String>(count);
            datalist = new ArrayList<JSONObject>(count);

            String filterableString = null;
            JSONObject value=null;

            for (int i = 0; i < count; i++) {
                try {
                    filterableString = list.get(i);
                    value= plantData.get(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(filterableString);
                    datalist.add(value);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<String>) results.values;
            notifyDataSetChanged();
        }

    }

}
*/
