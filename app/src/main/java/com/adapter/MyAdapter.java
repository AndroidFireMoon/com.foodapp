/*
package com.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.constants.Utils;
import com.foodapp.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

*/
/**
 * Created by firemoon on 10/5/17.
 *//*



public class MyAdapter extends BaseAdapter implements Filterable {
    private ItemFilter mFilter = new ItemFilter();
    private List<String> originalData = null;
    private List<String>filteredData = null;
    private LayoutInflater mInflater;
    Activity ctx;
    ArrayList<JSONObject> datalist,plantData;
    ArrayList<String> subplantData;
    public MyAdapter(Activity context, ArrayList<JSONObject> datalist, ArrayList<String> nameSearch){
        ctx=context;
        this.plantData=datalist;
        subplantData=nameSearch;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return subplantData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return subplantData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        TextView food_name, tv2, tv3;
        ImageView food_image;
        LayoutInflater inflater = ctx.getLayoutInflater();
        View row1 = inflater.inflate(R.layout.categories, parent, false);
        food_name= (TextView) row1.findViewById(R.id.food_name);
        food_image= (ImageView) row1.findViewById(R.id.food_image);

        try {
            food_name.setText(subplantData.get(position));

            UrlImageViewHelper.setUrlDrawable(food_image, Utils.encodeURL(plantData.get(position).getString("cat_img")),
                    R.mipmap.recplaceholder, new UrlImageViewCallback() {
                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {

                            if (!loadedFromCache) {
                                ScaleAnimation scale = new ScaleAnimation(0, 1,
                                        0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f);
                                scale.setDuration(300);
                                scale.setInterpolator(new OvershootInterpolator());
                                imageView.startAnimation(scale);
                            }

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return row1;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<String> list = subplantData;

            int count = list.size();
            final ArrayList<String> nlist = new ArrayList<String>(count);
            datalist = new ArrayList<JSONObject>(count);

            String filterableString = null;
            JSONObject value=null;

            for (int i = 0; i < count; i++) {
                try {
                    filterableString = list.get(i);
                    value= plantData.get(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(filterableString);
                    datalist.add(value);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            subplantData = (ArrayList<String>) results.values;
            notifyDataSetChanged();
        }

    }

}*/
