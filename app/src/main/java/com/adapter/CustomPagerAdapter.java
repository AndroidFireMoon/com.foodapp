package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.constants.AsyncTaskJSONObject;
import com.constants.Utils;
import com.villasfood.R;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import java.util.ArrayList;

/**
 * Created by firemoon on 2/5/17.
 */

public class CustomPagerAdapter extends PagerAdapter {

    Activity mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> mResources;
    AsyncTaskJSONObject.AsynctaskListner callBackListner;
    boolean flag;

    public CustomPagerAdapter(Activity context, ArrayList<String> imagesLink,AsyncTaskJSONObject.AsynctaskListner callBackListner,boolean flag) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResources = imagesLink;
        this.callBackListner = callBackListner;
        this.flag = flag;

    }


    @Override
    public int getCount(){
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.viewpagerlayout, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);





        UrlImageViewHelper.setUrlDrawable(imageView, Utils.encodeURL(mResources.get(position)),
                0, new UrlImageViewCallback() {
                    @Override
                    public void onLoaded(ImageView imageView,
                                         Bitmap loadedBitmap, String url,
                                         boolean loadedFromCache) {

                        if (!loadedFromCache) {
                            ScaleAnimation scale = new ScaleAnimation(0, 1,
                                    0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                    .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                    .5f);
                            scale.setDuration(300);
                            scale.setInterpolator(new OvershootInterpolator());
                            imageView.startAnimation(scale);
                        }
                    }
                });






        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }




}
