package com.fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adapter.CustomPagerAdapter;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseFragment;
import com.constants.CommonUtils;
import com.constants.SimpleHTTPConnection;
import com.constants.Utils;
import com.villasfood.CategoryListCheff;
import com.villasfood.LoginActivity;
import com.villasfood.ProfileActivity;
import com.villasfood.R;
import com.villasfood.UserOrders;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by firemoon on 3/5/17.
 */




public class CheffFragment extends BaseFragment implements AsyncTaskJSONObject.AsynctaskListner {
    CustomPagerAdapter pagerAdapter;
    public static final int GALLERY_INTENT_CALLED = 223424;
    public static final int GALLERY_KITKAT_INTENT_CALLED = 55000;
    public static final int CUSTOM_REQUEST_CODE = 39400;
    View view;
    ViewPager menu_images;
    RelativeLayout viewpager;
    TextView signin;
    Toolbar mToolbar;
    TextView  config,order,catalog,report;
    ArrayList<String> images=new ArrayList<>();
    Button uploadImage;
    String pathImage="";
    ArrayList<UrlImageViewHelper> rememb;
    CircleIndicator indicator;
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    ImageView deleteSpecial;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, null);
        context = getActivity();
        callBackListner=this;
        return view;

    }

    // viewpager


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mToolbar = (Toolbar) context.findViewById(R.id.toolbar);
        menu_images= (ViewPager) view.findViewById(R.id.menu_images);
//        tvHeader = (TextView)context.findViewById(R.id.tvHeader);
        signin= (TextView) view.findViewById(R.id.signin);
        config= (TextView) view.findViewById(R.id.configuration);
        order= (TextView) view.findViewById(R.id.order);
        catalog= (TextView) view.findViewById(R.id.catalog);
        report= (TextView) view.findViewById(R.id.report);
        uploadImage= (Button) view.findViewById(R.id.uploadImage);

        deleteSpecial= (ImageView) view.findViewById(R.id.deleteSpecial);


        signin.setTypeface(Utils.setFontText(context));
        config.setTypeface(Utils.setFontText(context));
        catalog.setTypeface(Utils.setFontText(context));
        order.setTypeface(Utils.setFontText(context));
        report.setTypeface(Utils.setFontText(context));


        order.setText("Order List");
        catalog.setText("Categories");
        catalog.setText("Categories");



        View singinview= (View) view.findViewById(R.id.singinview);
        viewpager= (RelativeLayout) view.findViewById(R.id.viewpager);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator);
        pagerAdapter=new CustomPagerAdapter(context,images,callBackListner,true);
        menu_images.setAdapter(pagerAdapter);
        indicator.setViewPager(menu_images);
        menu_images.setCurrentItem(0);


        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,LoginActivity.class));
            }
        });


        if(LoginActivity.checkCheff==true){

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );
            param.setMargins(0, 8, 0, 0);
            config.setLayoutParams(param);
            LoginActivity.checkCheff=false;
            singinview.setVisibility(View.GONE);
            signin.setVisibility(View.GONE);
        }

        catalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, CategoryListCheff.class));
            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(context, UserOrders.class));
            }
        });

/*
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,GenerateReport.class));
            }
        });
*/

        config.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,ProfileActivity.class));
            }
        });

        //  multiMillioniare
        //

      uploadImage.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialogForChoose(context);

        }
    });
        getItem();



        deleteSpecial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //deleteItem(menu_images.getCurrentItem());
            }
        });

    }

    protected void deleteItem(int position){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id", CommonUtils.getPreferencesString(context,"user_id"));
        if(position==0){
            params.put("splashkey", "splashone");
            Utils.write("entry          "+1);
        }else if(position==1){
            params.put("splashkey", "splashtwo");
            Utils.write("entry          "+2);
        }else if(position==2){
            params.put("splashkey", "splashthree");
            Utils.write("entry          "+3);

        }

        Utils.write("entry          "+CommonUtils.getPreferencesString(context,"user_id"));

        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.DELETE_ITEM);
    }
    protected void getItem(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.GET_ITEM);
    }

    @Override
    public void onResume() {
        super.onResume();

        mToolbar.setTitle("Menu");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    exitFromApp();
                    return true;
                }

                return false;
            }
        });
        uploadImage.setVisibility(View.VISIBLE);
       // deleteSpecial.setVisibility(View.VISIBLE);

    }

    // for -

    protected void exitFromApp() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setMessage("Do you want to exit from cheff section?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int id) {
      /*                  Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                        context.sendBroadcast(broadcastIntent);
                        context.finishAffinity();*/

                        context.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }


    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = android.app.Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {

        }
        sChildFragmentManagerField = f;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {

            }
        }

    }

// code for view pager slider


    public void ShowDialogForChoose(Activity context) {

        //


        final AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setAdapter(
                        new ArrayAdapter<String>(context,
                                android.R.layout.simple_list_item_1,
                                new String[]{
                                        "From Photo Gallery", "Cancel"}) {
                            public View getView(int position, View convertView,
                                                ViewGroup parent) {
                                View view = super.getView(position,
                                        convertView, parent);
                                TextView textView = (TextView) view
                                        .findViewById(android.R.id.text1);
                                textView.setTextColor(Color.BLACK);
                                return view;
                            }
                        }, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                 if (which == 0) {
                                    if (Build.VERSION.SDK_INT > 19) {
                                        Intent intent = new Intent();
                                        intent.setType("image/jpeg");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"), GALLERY_INTENT_CALLED);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                                        intent.setType("image/jpeg");
                                        startActivityForResult(intent, GALLERY_KITKAT_INTENT_CALLED);
                                    }
                                } else {
                                    dialog.dismiss();
                                }
                            }
                        });

        builder.create().show();



    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Intent dataVal = data;
            // Bitmap photo = (Bitmap) data.getExtras().get("data");
            // profilepic_addproduct.setImageBitmap(photo);

            if (requestCode == CUSTOM_REQUEST_CODE) {
                if (data.getData() != null) {
                    pathImage = getPath(context, data.getData());
                    pathImage = compressImage(pathImage);

                    File imgFile = new File(pathImage);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());



  /*                      if(typeOfPic.equalsIgnoreCase("image1")){
                            path1=pathImage;
                            image1.setImageBitmap(myBitmap);
                        }else if(typeOfPic.equalsIgnoreCase("image2")){
                            path2=pathImage;
                            image2.setImageBitmap(myBitmap);
                        }else{
                            path3=pathImage;
                            image3.setImageBitmap(myBitmap);
                        }*/
                    }

                } else {
                    Bitmap bitmap = dataVal.getExtras().getParcelable("data");
                    FileOutputStream out = null;
                    try {

                        String imagename = "img" + requestCode + ".jpg";
                        out = new FileOutputStream(new File(
                                Environment.getExternalStorageDirectory() + "/"
                                        + imagename));
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

                        pathImage = Environment.getExternalStorageDirectory() + "/"
                                + imagename;
                        pathImage = compressImage(pathImage);

                        File imgFile = new File(pathImage);

                        if (imgFile.exists()) {
                            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                                    .getAbsolutePath());

/*                            if(typeOfPic.equalsIgnoreCase("image1")){
                                path1=pathImage;
                                image1.setImageBitmap(myBitmap);
                            }else if(typeOfPic.equalsIgnoreCase("image2")){
                                path2=pathImage;
                                image2.setImageBitmap(myBitmap);
                            }else{
                                path3=pathImage;
                                image3.setImageBitmap(myBitmap);
                            }*/

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }

            } else if (data != null && requestCode == GALLERY_INTENT_CALLED
                    || requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                if (resultCode != Activity.RESULT_OK)
                    return;
                if (null == data)
                    return;
                Uri originalUri = null;
                if (requestCode == GALLERY_INTENT_CALLED) {
                    originalUri = data.getData();
                } else if (requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                    originalUri = data.getData();
                    final int takeFlags = data.getFlags()
                            & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    // Check for the freshest data.
                    context.getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
                }
                pathImage = getPath(context, originalUri);
                // path=compressImage(path + "");

//                Utils.write("check path===" + path);
                try {
                    pathImage = compressImage(pathImage);

                    File imgFile = new File(pathImage);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                                .getAbsolutePath());



                        /*if(typeOfPic.equalsIgnoreCase("image1")){
                            path1=pathImage;
                            image1.setImageBitmap(myBitmap);
                        }else if(typeOfPic.equalsIgnoreCase("image2")){
                            path2=pathImage;
                            image2.setImageBitmap(myBitmap);
                        }else{

                            path3=pathImage;
                            image3.setImageBitmap(myBitmap);
                        }*/


                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }


            Utils.write("pathImage       "+pathImage);
            if(pathImage!=null && pathImage.length()>0)
           new SubmitDataToServer(pathImage,context).execute();
            else
                Utils.showToastS(context,"Please choose image");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String compressImage(String imageUri) {

        String filePath1 = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // by setting this field as true, the actual bitmap pixels are not
        // loaded in the memory. Just the bounds are loaded. If
        // you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath1, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        // max Height and width values of the compressed image is taken as
        // 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the
        // image

        System.out.println("...............1  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                System.out.println("a.................");
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                System.out.println("a.................");
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                System.out.println("c.................");
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        System.out.println("...............2  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);
        // setting inSampleSize value allows to load a scaled down version of
        // the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth,
                actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low
        // on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath1, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath1);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            // write the compressed bitmap at the destination specified by
            // filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("...............3  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".jpg");
        System.out.println("uriSting-----------------" + uriSting);
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null,
                null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options,
                                     int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open("WineryApp.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressLint("NewApi")
    public String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getDataColumn(Context context, Uri uri, String selection,
                                String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

// multipart entity
ByteArrayBody bab;

    // service request
    public ProgressDialog dialog;
    public class SubmitDataToServer extends
            AsyncTask<Void, Integer, String> {
        String datacheck;
        Context cont;


        public SubmitDataToServer(String ctx, Activity context) {
            datacheck = ctx;
            cont=context;
            dialog = new ProgressDialog(cont);

        }

        @Override
        protected String doInBackground(Void... params) {


            MultipartEntity mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            HttpPost httppost = new HttpPost(AppConstant.UPLOAD_ITEM);

            try {
                mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);


                    Bitmap bm = BitmapFactory.decodeFile(pathImage);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                    byte[] data = bos.toByteArray();
                    bab = new ByteArrayBody(data, "forest.jpg");
                    mp.addPart("image",bab);
                System.out.println("sop file======================="+bab);



              /*  if(pathImage!=null){
                    File file = new File(pathImage);
                    Log.d("EDIT USER PROFILE", "UPLOAD: file length = " + file.length());
                    Log.d("EDIT USER PROFILE", "UPLOAD: file exist = " + file.exists());
                    mp.addPart("image", new FileBody(file, "application/octet"));

                    Utils.write("entry inside file==================="+new FileBody(file, "application/octet"));
                    System.out.println("sop file=======================");
                }*/

                mp.addPart("user_id",new StringBody(CommonUtils.getPreferencesString(context,"user_id").trim(),
                        "text/plain", Charset.forName("UTF-8")));


                if(menu_images.getCurrentItem()==0){
                    Utils.write("entry inside file===================0");
                    System.out.println("sop file=======================0");

                    mp.addPart("splashkey",new StringBody("splashone","text/plain",Charset.forName("UTF-8")));
                }else if(menu_images.getCurrentItem()==1){
                    Utils.write("entry inside file===================1");
                    System.out.println("sop file=======================1");
                    mp.addPart("splashkey",new StringBody("splashtwo","text/plain",Charset.forName("UTF-8")));
                }else if(menu_images.getCurrentItem()==2){
                    Utils.write("entry inside file===================2");
                    System.out.println("sop file=======================2");
                    mp.addPart("splashkey",new StringBody("splashthree","text/plain",Charset.forName("UTF-8")));
                }else if(menu_images.getCurrentItem()==3){
                    Utils.write("entry inside file===================3");
                    System.out.println("sop file=======================3");
                    mp.addPart("splashkey",new StringBody("splashfour","text/plain",Charset.forName("UTF-8")));
                }
                else if(menu_images.getCurrentItem()==4){
                    Utils.write("entry inside file===================4");
                    System.out.println("sop file=======================4");
                    mp.addPart("splashkey",new StringBody("splashfive","text/plain",Charset.forName("UTF-8")));
                }


                Utils.write("pathImage           "+pathImage);
                Utils.write("pathImage           "+CommonUtils.getPreferencesString(context,"user_id"));
                Utils.write("pathImage           "+menu_images.getCurrentItem());



            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            httppost.setEntity(mp);
            return SimpleHTTPConnection.sendByPOST(httppost);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setCancelable(false);
            dialog.setMessage("Sending data to admin..");
            dialog.show();

        }

        @Override
        protected void onPostExecute(String result) {
            images.clear();
            System.out.println("UPDATE USER PROFILE VALUES===" + result);
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (result != null) {
                try {
                    JSONObject object=new JSONObject(result);

                    if(object.getString("error").equalsIgnoreCase("false")){
                        CommonUtils.showToast(context,"Submitted successfully");

                        JSONObject obj=object.getJSONArray("data").getJSONObject(0);
                       if(!obj.getString("splash_img1").equalsIgnoreCase(""))
                        images.add(obj.getString("splash_img1"));

                        if(!obj.getString("splash_img2").equalsIgnoreCase(""))
                        images.add(obj.getString("splash_img2"));

                        if(!obj.getString("splash_img3").equalsIgnoreCase(""))
                        images.add(obj.getString("splash_img3"));

                        if(!obj.getString("splash_img4").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img4"));

                        if(!obj.getString("splash_img5").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img5"));


                        pagerAdapter=new CustomPagerAdapter(context,images,callBackListner,true);
                        menu_images.setAdapter(pagerAdapter);
                        indicator.setViewPager(menu_images);
                        menu_images.setCurrentItem(0);

//                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }
    }



    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {

        try{
            images.clear();

            Utils.write("arr         "+arr);

            if(action.equalsIgnoreCase(AppConstant.GET_ITEM)){

                try {
                    JSONObject object=new JSONObject(arr);

                    if(object.getString("error").equalsIgnoreCase("false")){
                        CommonUtils.showToast(context,"Submitted successfully");

                        JSONObject obj=object.getJSONArray("data").getJSONObject(0);
                        if(!obj.getString("splash_img1").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img1"));

                        if(!obj.getString("splash_img2").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img2"));

                        if(!obj.getString("splash_img3").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img3"));

                        if(!obj.getString("splash_img4").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img4"));

                        if(!obj.getString("splash_img5").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img5"));


                        pagerAdapter=new CustomPagerAdapter(context,images,callBackListner,true);
                        menu_images.setAdapter(pagerAdapter);
                        indicator.setViewPager(menu_images);
                        menu_images.setCurrentItem(0);

//                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }else if(action.equalsIgnoreCase(AppConstant.DELETE_ITEM)){

                try {
                    JSONObject object=new JSONObject(arr);

                    if(object.getString("error").equalsIgnoreCase("false")){
                        CommonUtils.showToast(context,"Submitted successfully");

                        JSONObject obj=object.getJSONArray("data").getJSONObject(0);
                        if(!obj.getString("splash_img1").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img1"));

                        if(!obj.getString("splash_img2").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img2"));

                        if(!obj.getString("splash_img3").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img3"));


                        pagerAdapter=new CustomPagerAdapter(context,images,callBackListner,true);
                        menu_images.setAdapter(pagerAdapter);
                        indicator.setViewPager(menu_images);
                        menu_images.setCurrentItem(0);

//                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }catch (Exception e){
            e.printStackTrace();
            //  menu_item.setAdapter(new MenuCorouselAdapter(context,objectItem, nameSearch));
        }
    }




}
