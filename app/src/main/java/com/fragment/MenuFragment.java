package com.fragment;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adapter.CustomPagerAdapter;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseFragment;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.villasfood.LoginUser;
import com.villasfood.MyOrders;
import com.villasfood.OrderDetailUserFragment;
import com.villasfood.ProfileForUser;
import com.villasfood.R;

import org.json.JSONObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import me.relex.circleindicator.CircleIndicator;


/**
 * Created by firemoon on 20/3/17.
 */
public class MenuFragment extends BaseFragment implements AsyncTaskJSONObject.AsynctaskListner {
    View view;
    ViewPager menu_images;
    RelativeLayout viewpager;
    TextView signin;
    Toolbar mToolbar;
    TextView  config,order,catalog,report;
    ArrayList<String> images=new ArrayList<>();
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    CustomPagerAdapter customRef;
    CircleIndicator indicator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, null);
        context = getActivity();
        callBackListner=this;
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

         mToolbar = (Toolbar) context.findViewById(R.id.toolbar);
         menu_images= (ViewPager) view.findViewById(R.id.menu_images);
         signin= (TextView) view.findViewById(R.id.signin);
         config= (TextView) view.findViewById(R.id.configuration);
         order= (TextView) view.findViewById(R.id.order);
         catalog= (TextView) view.findViewById(R.id.catalog);
         report= (TextView) view.findViewById(R.id.report);
         signin.setTypeface(Utils.setFontText(context));
         config.setTypeface(Utils.setFontText(context));
         catalog.setTypeface(Utils.setFontText(context));
         order.setTypeface(Utils.setFontText(context));
         report.setTypeface(Utils.setFontText(context));



        View singinview= (View) view.findViewById(R.id.singinview);
        viewpager= (RelativeLayout) view.findViewById(R.id.viewpager);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator);
        customRef=new CustomPagerAdapter(context,images,callBackListner,false);
        menu_images.setAdapter(customRef);
        indicator.setViewPager(menu_images);
        menu_images.setCurrentItem(2);




        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("entry worling  menu");

                startActivity(new Intent(context,LoginUser.class));
                getActivity().finish();
            }
        });


        if(CommonUtils.getPreferencesString(context,"check").equalsIgnoreCase("success")){

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );
            param.setMargins(0, 8, 0, 0);
            config.setLayoutParams(param);
            singinview.setVisibility(View.GONE);
            signin.setVisibility(View.GONE);

        }

        catalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(CommonUtils.getPreferencesString(context,"check").equalsIgnoreCase("success"))
                    startActivity(new Intent(context,OrderDetailUserFragment.class));
                else {
                    startActivity(new Intent(context, LoginUser.class));
                    getActivity().finish();
                }
                //startActivity(new Intent(context, OrderDetailUserFragment.class));

            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(context, MyOrders.class));


                if(CommonUtils.getPreferencesString(context,"check").equalsIgnoreCase("success"))
                    startActivity(new Intent(context,MyOrders.class));
                else {
                    startActivity(new Intent(context, LoginUser.class));
                    getActivity().finish();
                }



            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(context,GenerateReport.class));


/*                if(CommonUtils.getPreferencesString(context,"check").equalsIgnoreCase("success"))
                    startActivity(new Intent(context,GenerateReport.class));
                else {
                    startActivity(new Intent(context, LoginUser.class));
                    getActivity().finish();
                }*/

            }
        });

        config.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CommonUtils.getPreferencesString(context,"check").equalsIgnoreCase("success"))
                startActivity(new Intent(context,ProfileForUser.class));
                else {
                    startActivity(new Intent(context, LoginUser.class));
                    getActivity().finish();
                }
            }
        });

        getItem();
    }


    @Override
    public void onResume() {
        super.onResume();

        mToolbar.setTitle("Menu");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    exitFromApp();
                    return true;

                }

                return false;
            }
        });

    }

    protected void exitFromApp() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);

        builder.setMessage("Do you want to exit from user section?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int id) {
                     /*   Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                        context.sendBroadcast(broadcastIntent);
                        context.finishAffinity();*/

                        context.finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }


    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {

        }
        sChildFragmentManagerField = f;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {

            }
        }



    }


    // get iitems

    protected void getItem(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.GET_ITEM);
    }

    ArrayList<JSONObject> objectDatabase = new ArrayList<>();
    ArrayList<JSONObject> objectItem = new ArrayList<>();

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {

        try{
            images.clear();

            Utils.write("arr         "+arr);

            if(action.equalsIgnoreCase(AppConstant.GET_ITEM)){

                try {
                    JSONObject object=new JSONObject(arr);

                    if(object.getString("error").equalsIgnoreCase("false")){
                      //  CommonUtils.showToast(context,"Submitted successfully");

                        JSONObject obj=object.getJSONArray("data").getJSONObject(0);
                        if(!obj.getString("splash_img1").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img1"));

                        if(!obj.getString("splash_img2").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img2"));

                        if(!obj.getString("splash_img3").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img3"));

                        if(!obj.getString("splash_img4").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img4"));

                        if(!obj.getString("splash_img5").equalsIgnoreCase(""))
                            images.add(obj.getString("splash_img5"));

                        customRef=new CustomPagerAdapter(context,images,callBackListner,false);
                        menu_images.setAdapter(customRef);
                        indicator.setViewPager(menu_images);
                        menu_images.setCurrentItem(0);

//                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


        }catch (Exception e){
            e.printStackTrace();
            //  menu_item.setAdapter(new MenuCorouselAdapter(context,objectItem, nameSearch));
        }
    }



}
