package com.villasfood;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.BaseActivity;
import com.constants.Utils;
import com.google.firebase.iid.FirebaseInstanceId;

public class Splash extends BaseActivity{


    private Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

                Intent intent = new Intent(Splash.this, StartUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            handler.removeCallbacks(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        try{

/*            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
                if (ActivityCompat.checkSelfPermission(Splash.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Splash.this, new String[]{android.Manifest.permission.INTERNET, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WAKE_LOCK}, 200);

                }
            }else{

                commonnMethod();
            }*/

            commonnMethod();

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    void commonnMethod(){
        AppConstant.token = FirebaseInstanceId.getInstance().getToken();
        handler.postDelayed(runnable, 3000);
        String android_id = Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID);
        Utils.write(android_id+"   token       "+AppConstant.token);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    commonnMethod();
                }else{
                    Utils.showToastS(context,"Please allow permission");
                }
                break;
        }
    }

}
