package com.villasfood;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;


import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.CustomTimePickerDialog;
import com.constants.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by firemoon on 29/4/17.
 */

public class CartScreen extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    TextView cartOpen,confiremOrder;
    ImageView cartIcon;
    ListView listOrders;
    String  cart_id_concat;
    ArrayList<String> cart_ids=new ArrayList<>();
    TextView addMore;
    String timeValue;
    String commentValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
   try{
       setContentView(R.layout.cart_list);
       getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);;
       setHeaderValue("Cart");
       context=this;
       callBackListner=this;
       drawerInit();
       back();
       addMore= (TextView) findViewById(R.id.addMore);
       confiremOrder= (TextView) findViewById(R.id.confireOrder);
       listOrders= (ListView) findViewById(R.id.list_data);
       confiremOrder.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ShowDialogForChoose();
           }
       });
       addMore.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               startActivity(new Intent(CartScreen.this,OrderDetailUserFragment.class).setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)));
               finish();
           }
       });

       if (CommonUtils.isNetworkConnected(context))
           makeVolley();
       else
           Utils.showToastS(context,"Network-Error");


        }catch (Exception e){
       e.printStackTrace();
     }
    }

    TimeSlot adapter;
    protected void ShowDialogForChoose() {


      final String place[]= new String[]{"","In My Room or Balcony","At the Jackfruit Tree Dinner","In the Dragon's Grotto by the River",
              "At the Terrace Theater by the River","At the Campfire Stones","By the Pool","I am requesting a Non-Meal Amenity"};

        adapter=new TimeSlot(context,place);
        final AlertDialog.Builder builder = new AlertDialog.Builder(CartScreen.this);

        builder.setTitle("ORDER PLACE SELECTION");

                       builder.setAdapter(
                       adapter, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                Utils.write("working     "+place[which]);

                                orderSubmission(place[which]);
                            }
                        });


         dialog = builder.create();
       dialog.show();





    }
    AlertDialog dialog;
    protected void makeVolley(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id",CommonUtils.getPreferencesString(context,"user_local_id"));
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.CART_LIST);
    }

    protected void orderSubmission(String s){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id",CommonUtils.getPreferencesString(context,"user_local_id"));
        params.put("cart_id",cart_id_concat);
        params.put("address",s);
        params.put("time_interval",timeValue);
        params.put("comment",commentValue);

        Utils.write("cart_id_concat     "+CommonUtils.getPreferencesString(context,"user_local_id"));
        Utils.write("cart_id_concat     "+cart_id_concat);
        Utils.write("cart_id_concat     "+s);
        Utils.write("cart_id_concat     "+timeValue);
        Utils.write("cart_id_concat     "+commentValue);

        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.CONFIRM_ORDER);
    }

     ArrayList<JSONObject> objectDatabase = new ArrayList<>();
     ArrayList<JSONObject> objectItem = new ArrayList<>();

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{

            System.out.println("entry is not available     "+arr);
            System.out.println("     "+arr);
            if(action.equalsIgnoreCase(AppConstant.CART_LIST)){
                objectDatabase.clear();
                JSONObject object=new JSONObject(arr);
                if(object.getString("error").equalsIgnoreCase("false")){
                JSONArray data=object.getJSONArray("data");

                    CommonUtils.savePreferencesString(context,"cart",String.valueOf(data.length()));

                for(int j=0;j<data.length();j++) {
                    cart_ids.add(data.getJSONObject(j).getString("cart_id"));
                    objectDatabase.add(data.getJSONObject(j));
                }
                    cart_id_concat=android.text.TextUtils.join("-", cart_ids);
                    Utils.write("data       "+cart_id_concat);
                    listOrders.setAdapter(new CartAdapter(context,objectDatabase));

                }else{
                    confiremOrder.setVisibility(View.GONE);
                    CommonUtils.savePreferencesString(context,"cart",String.valueOf(0));
                    CommonUtils.showToast(context,object.getString("message"));
                    finish();
                }
            }else if(action.equalsIgnoreCase(AppConstant.CLEAR_LIST)){
                /* objectDatabase.clear();
                    JSONObject object=new JSONObject(arr);
                     if(object.getString("error").equalsIgnoreCase("false")){
                    JSONArray data=object.getJSONArray("data");
                    for(int j=0;j<data.length();j++)
                        objectDatabase.add(data.getJSONObject(j));

                    listOrders.setAdapter(new CartAdapter(context,objectDatabase));

                CommonUtils.showToast(context,"Order removed successfully");
                }else{
                    CommonUtils.showToast(context,object.getString("message"));
                    lstOrders.setAdapter(null);
                }*/
                makeVolley();
            }else{
                JSONObject object=new JSONObject(arr);

                if(object.getString("error").equalsIgnoreCase("true")){
                Utils.showToastS(context,"Please choose appropriate time");
                }else{

                CommonUtils.savePreferencesString(context,"cart",String.valueOf(0));
                CommonUtils.showToast(context,"Order confirmed successfully");
                Intent i=new Intent(CartScreen.this,MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            confiremOrder.setVisibility(View.GONE);
            Utils.showToastS(context,"No Item Added in Cart");
            Intent i=new Intent(CartScreen.this,MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }



    public void removeCart(String cart_id, Activity ctx){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("cart_id",cart_id);
        new AsyncTaskJSONObject(true, params, callBackListner, ctx, AppConstant.CLEAR_LIST);

    }

    /**
     * Created by firemoon on 4/5/17.
     **/


    public class CartAdapter extends BaseAdapter {

        private List<JSONObject> originalData = new ArrayList<>();
        private List<String>filteredData = null;
        private LayoutInflater mInflater;
        int layout;
        Activity ctx;


        public CartAdapter(Activity context, ArrayList<JSONObject> objectDatabase){
//        layout=order_item_recycle;
            ctx=context;
            originalData=objectDatabase;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return originalData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return originalData.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            TextView tv1, tv2, tv3,tv4,tv5,tv6;
            LayoutInflater inflater =ctx.getLayoutInflater();
            View row1 = inflater.inflate(R.layout.cart_design, parent, false);
            try {
                tv1= (TextView) row1.findViewById(R.id.item_title);
                tv2= (TextView) row1.findViewById(R.id.qty_data);
                tv3= (TextView) row1.findViewById(R.id.price_item);
                tv4= (TextView) row1.findViewById(R.id.remove_order);
                tv5= (TextView) row1.findViewById(R.id.extracheeseprice);
                tv6= (TextView) row1.findViewById(R.id.pricedata);

                tv1.setText(originalData.get(position).getString("item_title"));
                tv2.setText(originalData.get(position).getString("quantity"));
                tv3.setText("RP "+originalData.get(position).getString("price"));
                tv6.setText("RP "+originalData.get(position).getString("price"));

                tv4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            removeCart(originalData.get(position).getString("cart_id"),ctx);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();

            }
            return row1;
        }
    }



    public class TimeSlot extends BaseAdapter {

        private List<JSONObject> originalData = new ArrayList<>();
        private List<String>filteredData = null;
        private LayoutInflater mInflater;
        int layout;
        Activity ctx;
        String[] dB;

        public TimeSlot(Activity context, String[] objectDatabase){
//        layout=order_item_recycle;
            ctx=context;
            dB=objectDatabase;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return dB.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return dB[position];
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            final TextView tv1, tv2, tv3,tv4,tv5,tv6,placeTitle;

            LayoutInflater inflater =ctx.getLayoutInflater();
            View row1 = inflater.inflate(R.layout.custom_time_picker, parent, false);
            try {
                tv1= (TextView) row1.findViewById(R.id.textPlace);
                tv2= (TextView) row1.findViewById(R.id.timePicker);
                placeTitle= (TextView) row1.findViewById(R.id.placeTitle);
                final EditText comment= (EditText) row1.findViewById(R.id.comment);

                comment.requestFocus();
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

                tv1.setText(dB[position]);


                if(position== 0){
                    comment.setVisibility(View.VISIBLE);
                    tv2.setVisibility(View.VISIBLE);
                    placeTitle.setVisibility(View.VISIBLE);
                   
                }else{
                    comment.setVisibility(View.GONE);
                    tv2.setVisibility(View.GONE);
                    placeTitle.setVisibility(View.GONE);
                }

                tv2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openPicker(tv2);
                    }
                });


                comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                    }
                });

                comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        Utils.write("data          "+s);
                        commentValue=""+s;
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();

            }
            return row1;
        }
    }


    protected  void openPicker(final TextView tv2){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
       /* mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                tv2.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();*/

        CustomTimePickerDialog refer=new CustomTimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                if(String.valueOf(selectedMinute).length()==1){
                    tv2.setText( selectedHour + ":" + selectedMinute+"0");
                }else{
                    tv2.setText( selectedHour + ":" + selectedMinute);
                }

                timeValue=tv2.getText().toString().trim();
            }
        }, hour, minute, true);
        refer.setTitle("Select Time");
        refer.show();

    }
}


