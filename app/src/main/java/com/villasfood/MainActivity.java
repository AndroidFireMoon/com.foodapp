package com.villasfood;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.villasfood.R;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.SessionManagement;
import com.constants.Utils;
import com.fragment.CheffFragment;
import com.fragment.MenuFragment;
import com.fragment.NavigationDrawerFragment;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;


public class MainActivity extends BaseActivity implements NavigationDrawerFragment.FragmentDrawerListener,AsyncTaskJSONObject.AsynctaskListner {
    SessionManagement sessionManagement;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        callBackListner=this;
        drawerInit();
        setHeaderValue("Menu");
        setNextActiveFragment(newInstance(),true);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }

    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
       /* args.putSerializable(CATEGORY, category);
        args.putInt(CATEGORY_CURRENT_POS, categoryCurrentPos);
        args.putInt(CATEGORY_TOTAL_COUNT, categoryTotalCount);
        args.putSerializable(SurveyActivity.SURVEY, selectedSurvey);*/
        fragment.setArguments(args);
        return fragment;
    }

        public static CheffFragment cheffInstance() {
        CheffFragment fragment = new CheffFragment();
        Bundle args = new Bundle();
       /* args.putSerializable(CATEGORY, category);
        args.putInt(CATEGORY_CURRENT_POS, categoryCurrentPos);
        args.putInt(CATEGORY_TOTAL_COUNT, categoryTotalCount);
        args.putSerializable(SurveyActivity.SURVEY, selectedSurvey);*/
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
               makeVolley();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
//
    protected void makeVolley(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id",CommonUtils.getPreferencesString(context,"user_local_id"));
        new AsyncTaskJSONObject(true, params, callBackListner, context, "http://myrx-u.com/foodapp/v1/logout");
        Utils.write("token               "+CommonUtils.getPreferencesString(context,"user_local_id"));
    }

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{

            Utils.write("token               "+arr);
            JSONObject object=new JSONObject(arr);
            Utils.showToastS(context,object.getString("message"));
            SessionManagement sessionManagement1= new SessionManagement(MainActivity.this);
            sessionManagement1.logoutUser();
           // CommonUtils.savePreferencesString(context,"email","");
          // CommonUtils.savePreferencesString(context,"from","");

            CommonUtils.savePreferencesString(context,"check","");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
