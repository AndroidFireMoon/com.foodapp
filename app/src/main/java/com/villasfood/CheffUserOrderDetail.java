package com.villasfood;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.adapter.AdapterFoodCheffDetail;
import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 3/5/17.
 */

public class CheffUserOrderDetail extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner
{
    Double totalAmount,totalBillingAmount;
    ArrayList<JSONObject> objectDatabase = new ArrayList<>();
    ListView listOrders;
    TextView t1,t2,t3;
    ArrayList<Double> referList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_detail_layout);
        try{
            context=this;
            back();
            drawerInit();
            setHeaderValue("Orders List");
            callBackListner=this;
            listOrders= (ListView) findViewById(R.id.list_data);

            if (CommonUtils.isNetworkConnected(context))
            userListOrder();
            else
                Utils.showToastS(context,"Network-Error");


   /*         View footerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.show_billing, null, false);
//            listOrders.addFooterView(footerView,null,false);
            t1= (TextView) footerView.findViewById(R.id.billing_number);
            t2= (TextView) footerView.findViewById(R.id.billing_amount);
            t3= (TextView) footerView.findViewById(R.id.billing_date);*/



        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void deleteOrder(String order_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("order_id",order_id);
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.Delete_Order);
    }

/*
    public void mapOrder(String cart_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("cart_id",cart_id);
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.MAPPING_ORDER);
    }
*/


    public Double sum(ArrayList<Double> list) {
        Double sum = 0.00;
        for (Double i : list)
            sum = sum + i;
        return sum;
    }
    // mobile engineer
    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{

            String billingDate = null;
            System.out.println("entry is not available     "+arr);
            System.out.println("     "+arr);
            if(action.equalsIgnoreCase(AppConstant.MAPPING_ORDER)){
                objectDatabase.clear();
                referList.clear();
                JSONObject object=new JSONObject(arr);
                JSONArray data=object.getJSONArray("data");
                for(int j=0;j<data.length();j++) {
                    objectDatabase.add(data.getJSONObject(j));
                    totalAmount=Double.parseDouble(objectDatabase.get(j).getString("price"));
                    referList.add(totalAmount);
                }

                try {
                    totalBillingAmount=sum(referList);
                  /*  t1.setText(""+data.length());
                    t2.setText("RP "+totalBillingAmount);
                    t3.setText(""+billingDate);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

                listOrders.setAdapter(new AdapterFoodCheffDetail(context,objectDatabase));
                listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                      //
                        try {
                            deleteAlert(objectDatabase.get(position).getString("order_id"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }else{
                userListOrder();
            }
        }catch (Exception e){
            e.printStackTrace();
            if(e!=null){
                Utils.showToastS(context,"No order available");
                finish();
            }
        }
    }


    protected void deleteAlert(final String order_id) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setMessage("Do you want to delete this item?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int id) {

                        deleteOrder(order_id);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }




    public void userListOrder(){

        Utils.write("carid        "+CommonUtils.getPreferencesString(context,"cart_id"));

        HashMap<String, Object> params = new LinkedHashMap<>();
//        params.put("user_id",CommonUtils.getPreferencesString(context,"user_list_id")); ORDER_LIST
        params.put("cart_id",CommonUtils.getPreferencesString(context,"cart_id"));
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.MAPPING_ORDER);
    }



    // time ()
}
