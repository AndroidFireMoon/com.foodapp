package com.villasfood;

import android.os.Bundle;
import android.widget.ListView;

import com.adapter.CustomListAdapter;
import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 2/5/17.
 */

public class MyOrders extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner{
    ListView listOrders;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.order_detail_layout);
        back();
        drawerInit();
        listOrders= (ListView) findViewById(R.id.list_data);
        setHeaderValue("My Orders");
        callBackListner=this;

        if (CommonUtils.isNetworkConnected(context))
        userListOrder();
        else
            Utils.showToastS(context,"Network-Error");

    }




    ArrayList<JSONObject> objectDatabase = new ArrayList<>();
    ArrayList<JSONObject> objectItem = new ArrayList<>();


    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{

            System.out.println("entry is not available     "+arr);
            System.out.println("     "+arr);
            if(action.equalsIgnoreCase(AppConstant.ORDER_LIST)){
                objectDatabase.clear();
                JSONObject object=new JSONObject(arr);
                if(object.getString("error").equalsIgnoreCase("false")){
                JSONArray data=object.getJSONArray("data");
                for(int j=0;j<data.length();j++) {
                    objectDatabase.add(data.getJSONObject(j));
                }
                listOrders.setAdapter(new CustomListAdapter(context,objectDatabase));
            }else{
                    CommonUtils.showToast(context,object.getString("message"));
                    finish();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Utils.showToastS(context,"No order available");
        }
    }



    public void userListOrder(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id", CommonUtils.getPreferencesString(context,"user_local_id"));
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.ORDER_LIST);
    }
}

