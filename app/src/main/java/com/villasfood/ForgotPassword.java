package com.villasfood;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.villasfood.R;
import com.constants.BaseActivity;
import com.constants.Utils;

/**
 * Created by firemoon on 20/3/17.
 */

public class ForgotPassword extends BaseActivity {
    EditText user_name_login;
    Button login_button_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_user);

        user_name_login= (EditText) findViewById(R.id.user_name_login);
        user_name_login.setHint("Please enter your email");
        login_button_user= (Button) findViewById(R.id.login_button_user);
        login_button_user.setText("SUBMIT");
        user_name_login.setTypeface(Utils.setFontText(ForgotPassword.this));
        login_button_user.setTypeface(Utils.setFontText(ForgotPassword.this));


        login_button_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showToastS(context,"sent an email to your account successfully.");
                finish();
            }
        });
    }
}
