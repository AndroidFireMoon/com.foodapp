package com.villasfood;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.fragment.MenuFragment;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by firemoon on 21/3/17.
 */

public class OrderDetailUserFragment extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner{
    View root;
    EditText et_search_offer;
    LinearLayout all_data;
    TextView food_name;
    Button food_detail_btn;
    ImageView plus,search;
    ListView listData;
    ArrayList<JSONObject> datalist;
    ArrayList<JSONObject> plantData=new ArrayList<>();
    ArrayList<String> subplantData=new ArrayList<>();
    MyAdapter myAdapter;
    boolean isShowing=true;
    ArrayList<JSONObject> categoryData=new ArrayList<>();
    ArrayList<String> nameSearch=new ArrayList<>();
    ArrayList<String> imageData=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    try{
    setContentView(R.layout.order_detail_layout);
    drawerInit();
    setHeaderValue("Order");
    back();
    listData= (ListView) findViewById(R.id.list_data);
    all_data= (LinearLayout) context.findViewById(R.id.all_data);
    LinearLayout lefttool= (LinearLayout) context.findViewById(R.id.righttool);
    lefttool.setVisibility(View.VISIBLE);
    plus= (ImageView) context.findViewById(R.id.addCart);
    search= (ImageView) context.findViewById(R.id.search);
    et_search_offer= (EditText) findViewById(R.id.et_search_offer);
    plus.setVisibility(View.VISIBLE);
    search.setVisibility(View.VISIBLE);


    listData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Utils.write("position     "+position);
            startActivity(new Intent(OrderDetailUserFragment.this,MenuCorousel.class).putExtra("position",String.valueOf(position)));
        }
    });

    plus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(OrderDetailUserFragment.this,CartScreen.class));
        }
    });



    search.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(isShowing==true) {
                isShowing=false;
                et_search_offer.setVisibility(View.VISIBLE);
            }else {
                isShowing=true;
                et_search_offer.setVisibility(View.GONE);
            }
        }
    });

    et_search_offer.addTextChangedListener(new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            System.out.println("Text ["+s+"]");
            try{
                myAdapter.getFilter().filter(s.toString());
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    });

   }catch(Exception e){
    e.printStackTrace();
       }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
       /* args.putSerializable(CATEGORY, category);
        args.putInt(CATEGORY_CURRENT_POS, categoryCurrentPos);
        args.putInt(CATEGORY_TOTAL_COUNT, categoryTotalCount);
        args.putSerializable(SurveyActivity.SURVEY, selectedSurvey);*/
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void onResume() {
        super.onResume();
        datalist=new ArrayList<>();
        if (CommonUtils.isNetworkConnected(context))
        makeVolley();
        else
            Utils.showToastS(context,"Network-Error");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                //  logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    protected void makeVolley(){
        callBackListner = OrderDetailUserFragment.this;
        HashMap<String, Object> params = new LinkedHashMap<>();
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.CAT_LIST);
    }


    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{
            System.out.println("arr        "+arr);
            if(action.equalsIgnoreCase(AppConstant.CAT_LIST)){
                categoryData.clear();
                nameSearch.clear();
                imageData.clear();

                JSONObject object=new JSONObject(arr);
                if(object.getString("error").equalsIgnoreCase("false")) {
                    JSONArray data = object.getJSONArray("data");
                    for (int j = 0; j < data.length(); j++) {
                        categoryData.add(data.getJSONObject(j));
                        nameSearch.add(data.getJSONObject(j).getString("cat_title"));
                        imageData.add(data.getJSONObject(j).getString("cat_img"));
                    }
                    myAdapter = new MyAdapter(context,categoryData,nameSearch);
                    listData.setAdapter(myAdapter);
                }else{

                    CommonUtils.showToast(context,object.getString("message"));
                }

            }

        }catch (Exception e){
            e.printStackTrace();
            Utils.showToastS(context,"No Catalog available");
           // finish();
        }

    }






    public class MyAdapter extends BaseAdapter implements Filterable {
         ItemFilter mFilter = new ItemFilter();
        private List<String> originalData = null;
        private List<String>filteredData = null;
        private LayoutInflater mInflater;
        Activity ctx;
        ArrayList<JSONObject> datalist,plantData;
        ArrayList<String> subplantData;
        public MyAdapter(Activity context, ArrayList<JSONObject> datalist, ArrayList<String> nameSearch){
            ctx=context;
            this.plantData=datalist;
            subplantData=nameSearch;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return subplantData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return subplantData.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            TextView food_name, tv2, tv3;
            ImageView food_image;
            LayoutInflater inflater = ctx.getLayoutInflater();
            View row1 = inflater.inflate(R.layout.categories, parent, false);
            food_name= (TextView) row1.findViewById(R.id.food_name);
            food_image= (ImageView) row1.findViewById(R.id.food_image);

            try {
                food_name.setText(subplantData.get(position));

                UrlImageViewHelper.setUrlDrawable(food_image, Utils.encodeURL(plantData.get(position).getString("cat_img")),
                        R.mipmap.recplaceholder, new UrlImageViewCallback() {
                            @Override
                            public void onLoaded(ImageView imageView,
                                                 Bitmap loadedBitmap, String url,
                                                 boolean loadedFromCache) {
                                if (!loadedFromCache) {
                                    ScaleAnimation scale = new ScaleAnimation(0, 1,
                                            0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f);
                                    scale.setDuration(300);
                                    scale.setInterpolator(new OvershootInterpolator());
                                    imageView.startAnimation(scale);
                                }

                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return row1;
        }

        @Override
        public Filter getFilter() {
            return mFilter;
        }


        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<String> list = nameSearch;

                int count = list.size();
                final ArrayList<String> nlist = new ArrayList<String>(count);
                datalist = new ArrayList<JSONObject>(count);

                String filterableString = null;
                JSONObject value=null;

                for (int i = 0; i < count; i++) {
                    try {
                        filterableString = list.get(i);
                        value= plantData.get(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(filterableString);
                        datalist.add(value);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();
                return results;
            }
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                subplantData = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}

