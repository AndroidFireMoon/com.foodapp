package com.villasfood;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.adapter.ItemListCheffAdapter;
import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 3/5/17.
 */

public class AddEditCategoryItemList extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    ArrayList<JSONObject> categoryData=new ArrayList<>();
    ListView listOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        try{
            context=this;
            setContentView(R.layout.order_detail_layout);
            back();
            drawerInit();
            setHeaderValue("Item List");
            listOrders= (ListView) findViewById(R.id.list_data);
            ImageView addCart= (ImageView) findViewById(R.id.addCartt);
           // addCart.setImageResource(R.mipmap.plus);
            addCart.setVisibility(View.VISIBLE);

            addCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context,AddEditCategoryItem.class).putExtra("heading","Add Item"));
                }
            });
            listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // startActivity(new Intent(AddEditCategoryItemList.this,AddEditCategoryItem.class));
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (CommonUtils.isNetworkConnected(context))
        makeVolley();
        else
            Utils.showToastS(context,"Network-Error");
    }

    protected void makeVolley(){
        callBackListner = AddEditCategoryItemList.this;
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("cat_id", CommonUtils.getPreferencesString(context,"cat_id"));
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.LIST_ITEM);
    }

    // mindset

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{
            if(action.equalsIgnoreCase(AppConstant.LIST_ITEM)){
                   categoryData.clear();
                   JSONObject object=new JSONObject(arr);

                   if(object.getString("error").equalsIgnoreCase("false")) {
                    JSONArray data = object.getJSONArray("data");
                        for (int j = 0; j < data.length(); j++)
                        categoryData.add(data.getJSONObject(j));

                       listOrders.setAdapter(new ItemListCheffAdapter(context, categoryData,callBackListner));
                }else{
                    CommonUtils.showToast(context,object.getString("message"));
                }
            }else if(action.equalsIgnoreCase(AppConstant.DELETE_ITM)){
                makeVolley();
            }
        }catch (Exception e){
            e.printStackTrace();

            if(e!=null){
                addItemDialouge();
            }
        }
    }

    protected void addItemDialouge() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setMessage("No item in this category")
                .setCancelable(false)
                .setPositiveButton("ADD ITEM", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(context,AddEditCategoryItem.class).putExtra("heading","Add Item"));
                    }
                })
                .setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
