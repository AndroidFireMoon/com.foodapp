package com.villasfood;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 29/4/17.
 */

public class DetailFood extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    TextView cartOpen,canceldata;
    ImageView cart,radio_box,food_icon;
    String tick="unselected",radio="unselected";
    TextView quantity,foodname,price;
//    ImageView decrease,increase;
    String stringVal;
    int money;
    LinearLayout plusclick,minusclick;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            setContentView(R.layout.specific_menu_detail);
            setHeaderValue("Burger");
            drawerInit();
            back();
            callBackListner=this;
            LinearLayout lefttool= (LinearLayout) context.findViewById(R.id.righttool);
            lefttool.setVisibility(View.VISIBLE);
            cart= (ImageView) findViewById(R.id.addCart);
          //  cart.setVisibility(View.VISIBLE);
            cartOpen= (TextView) findViewById(R.id.cartOpen);
            canceldata= (TextView) findViewById(R.id.canceldata);
            quantity= (TextView) findViewById(R.id.quantity);
            foodname= (TextView) findViewById(R.id.foodname);
            price= (TextView) findViewById(R.id.price);
            minusclick = (LinearLayout) findViewById(R.id.minusclick);
            plusclick = (LinearLayout) findViewById(R.id.plusclick);
            radio_box = (ImageView) findViewById(R.id.radio_box);
            food_icon = (ImageView) findViewById(R.id.food_icon);


            radio_box.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(radio.equalsIgnoreCase("selected")){
                        radio="unselected";
                        radio_box.setImageResource(R.mipmap.radiounselect);
                    }else{
                        radio="selected";
                        radio_box.setImageResource(R.mipmap.radioselect);
                    }


                }
            });

            cartOpen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CommonUtils.isNetworkConnected(context))
                        makeVolley();
                    else
                        Utils.showToastS(context,"Network-Error");

                }
            });

            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(DetailFood.this,CartScreen.class));
                }
            });

            canceldata.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            minusclick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(MenuCorousel.counter>1){
                        MenuCorousel.counter = Integer.parseInt(quantity.getText().toString());
                        MenuCorousel.counter-=1;
                        stringVal = Integer.toString(MenuCorousel.counter);
                        quantity.setText(stringVal);
                        money=  Integer.parseInt(CommonUtils.getPreferencesString(context,"item_price")) * Integer.parseInt(quantity.getText().toString().trim());
                        price.setText("RP   "+money);
                    }
                }
            });

            plusclick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuCorousel.counter = Integer.parseInt(quantity.getText().toString());
                    MenuCorousel.counter+=1;
                    stringVal = Integer.toString(MenuCorousel.counter);
                    quantity.setText(stringVal);

                    money=  Integer.parseInt(CommonUtils.getPreferencesString(context,"item_price")) * Integer.parseInt(quantity.getText().toString().trim());
                    price.setText("RP   "+money);
                }
            });



            if(MenuCorousel.counter==0)
                quantity.setText("1");
            else
                quantity.setText(String.valueOf(MenuCorousel.counter
                ));

            money=  Integer.parseInt(CommonUtils.getPreferencesString(context,"item_price")) * Integer.parseInt(quantity.getText().toString().trim());
            price.setText("RP   "+money);
            foodname.setText(CommonUtils.getPreferencesString(context,"item_title"));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    
    @Override
    protected void onResume() {
        super.onResume();
        UrlImageViewHelper.setUrlDrawable(food_icon, Utils.encodeURL(CommonUtils.getPreferencesString(context,"item_img")),
                R.mipmap.recplaceholder, new UrlImageViewCallback() {
                    @Override
                    public void onLoaded(ImageView imageView,
                                         Bitmap loadedBitmap, String url,
                                         boolean loadedFromCache) {

                        if (!loadedFromCache) {
                            ScaleAnimation scale = new ScaleAnimation(0, 1,
                                    0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                    .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                    .5f);
                            scale.setDuration(300);
                            scale.setInterpolator(new OvershootInterpolator());
                            imageView.startAnimation(scale);
                        }
                    }
                }) ;
    }

    protected void makeVolley(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("item_id",CommonUtils.getPreferencesString(context,"item_id"));
        params.put("user_id",CommonUtils.getPreferencesString(context,"user_local_id"));
        params.put("price",String.valueOf(money));
        params.put("quantity",quantity.getText().toString().trim());
        params.put("extra_cheese","");
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.ADD_CART);
    }

    ArrayList<JSONObject> objectDatabase = new ArrayList<>();
    ArrayList<JSONObject> objectItem = new ArrayList<>();
    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{
            System.out.println("     "+arr);
            if(action.equalsIgnoreCase(AppConstant.ADD_CART)){
                objectDatabase.clear();
                JSONObject object=new JSONObject(arr);
                JSONArray data=object.getJSONArray("data");
                for(int j=0;j<data.length();j++)
                    objectDatabase.add(data.getJSONObject(j));

                startActivity(new Intent(DetailFood.this,CartScreen.class));
                finish();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
