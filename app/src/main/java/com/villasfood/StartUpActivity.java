package com.villasfood;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by firemoon on 20/3/17.
 */

public class StartUpActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startupactivity);
        clicking();
        AppConstant.token = FirebaseInstanceId.getInstance().getToken();
    }

    public void gotoInside(){

        Utils.write("email------------>"+CommonUtils.getPreferencesString(context,"email"));

        if(!CommonUtils.getPreferencesString(context,"email").equalsIgnoreCase("")){
            System.out.println("entry done inside");
            LoginActivity.checkCheff=true;
            startActivity(new Intent(StartUpActivity.this,BackUpActivityCheff.class));
          //  finish();
        }else{
            startActivity(new Intent(context,LoginActivity.class));
            //finish();
        }

    }



    private void clicking() {

        TextView gotoCheff= (TextView) findViewById(R.id.gotoCheff);
        TextView gotoUser= (TextView) findViewById(R.id.gotoUser);
        gotoCheff.setTypeface(Utils.setFontText(StartUpActivity.this));
        gotoUser.setTypeface(Utils.setFontText(StartUpActivity.this));


        gotoCheff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("clicking 1");
                AppConstant.profileFlag="cheff";
                gotoInside();

            }
        });
        gotoUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("clicking 2");
                AppConstant.profileFlag="user";
                startActivity(new Intent(context,MainActivity.class));
            }
        });
    }



    //write the following code in oncreate method or whereever you want to use this


}
