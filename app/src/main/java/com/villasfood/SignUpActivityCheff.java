package com.villasfood;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.villasfood.R;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 17/3/17.
 */

public class SignUpActivityCheff extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner{
    EditText cheff_email,cheff_confirm_email,cheff_name,cheff_lastname,cheff_country,cheff_referrer, cheff_password;
    Button submit;
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    String array;

    @Override
    public void AsynctaskResult(String arr, String action,
                                HashMap<String, Object> webservices_params, boolean status) {
        try{            array=arr;
            System.out.println("arr       "+arr);

            if(action.equalsIgnoreCase("http://myrx-u.com/foodapp/v1/createNewUser")){


                JSONObject object=new JSONObject(arr);
                if(object.getString("error").equalsIgnoreCase("false")){
                    if (ActivityCompat.checkSelfPermission(SignUpActivityCheff.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SignUpActivityCheff.this, new String[]{Manifest.permission.INTERNET,Manifest.permission.ACCESS_NETWORK_STATE}, 200);
                    }else{
                        CommonUtils.savePreferencesString(context,"data",arr);
                        startActivity(new Intent(SignUpActivityCheff.this, LoginActivity.class));
                    }

                }
                else
                    CommonUtils.showToast(context,object.getString("Message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case 200:
                System.out.println("arr       "+array);
                CommonUtils.savePreferencesString(context,"data",array);
                startActivity(new Intent(SignUpActivityCheff.this, MainActivity.class));
                break;
        }

    }

    protected void makeVolley(){
        callBackListner = SignUpActivityCheff.this;
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("firstname", cheff_name.getText().toString().trim());
        params.put("lastname", cheff_lastname.getText().toString().trim());
        params.put("email", cheff_email.getText().toString().trim());
        params.put("password", cheff_password.getText().toString().trim());
        params.put("country", cheff_country.getText().toString().trim());
        params.put("reference", cheff_referrer.getText().toString().trim());
        new AsyncTaskJSONObject(true, params, callBackListner, context, "http://myrx-u.com/foodapp/v1/createNewUser");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cheff_sign_up);
        context=SignUpActivityCheff.this;
        findIDClicking();
    }



    private boolean checkValidation() {

        if (cheff_name.getText().toString().trim().length() == 0) {
            cheff_name.setError("Please enter First Name");
            cheff_name.requestFocus();
            return false;
        } else if (cheff_lastname.getText().toString().trim().length() == 0) {
            cheff_lastname.setError("Please enter Last Name");
            cheff_lastname.requestFocus();
            return false;
        } else if(cheff_email.getText().toString().trim().length() == 0) {
            cheff_email.setError("Please enter Email id");
            cheff_email.requestFocus();
            return false;
        } else if (!CommonUtils.isValidEmail(cheff_confirm_email.getText().toString().trim())) {
            cheff_confirm_email.setError("Please enter valid Email id");
            cheff_confirm_email.requestFocus();
            return false;
        }else if (cheff_country.getText().toString().trim().length() == 0) {
            cheff_country.setError("Please enter your Country");
            cheff_country.requestFocus();
            return false;
        }else if (cheff_referrer.getText().toString().trim().length() == 0) {
            cheff_referrer.setError("Please enter Referrar");
            cheff_referrer.requestFocus();
            return false;
        }else if (cheff_password.getText().toString().trim().length() == 0) {
            cheff_password.setError("Please enter password");
            cheff_password.requestFocus();
            return false;
        } else if (cheff_password.getText().toString().trim().length() < 8) {
            cheff_password.setError("Minimum length of password should be of 8 characters.");
            cheff_password.requestFocus();
            return false;

        } else {
            return true;
        }
    }



    private void findIDClicking() {
        cheff_email= (EditText) findViewById(R.id.cheff_email);
        cheff_confirm_email= (EditText) findViewById(R.id.cheff_confirm_email);
        cheff_name= (EditText) findViewById(R.id.cheff_name);
        cheff_lastname= (EditText) findViewById(R.id.cheff_lastname);
        cheff_country= (EditText) findViewById(R.id.cheff_country);
        cheff_referrer= (EditText) findViewById(R.id.cheff_referrer);
        cheff_password= (EditText) findViewById(R.id.cheff_password);
        submit= (Button) findViewById(R.id.submit);


        submit.setTypeface(Utils.setFontText(SignUpActivityCheff.this));
        cheff_email.setTypeface(Utils.setFontText(SignUpActivityCheff.this));
        cheff_confirm_email.setTypeface(Utils.setFontText(SignUpActivityCheff.this));
        cheff_name.setTypeface(Utils.setFontText(SignUpActivityCheff.this));
        cheff_lastname.setTypeface(Utils.setFontText(SignUpActivityCheff.this));
        cheff_country.setTypeface(Utils.setFontText(SignUpActivityCheff.this));
        cheff_referrer.setTypeface(Utils.setFontText(SignUpActivityCheff.this));
        cheff_password.setTypeface(Utils.setFontText(SignUpActivityCheff.this));



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isNetworkConnected(SignUpActivityCheff.this) && checkValidation()==true) {
                    makeVolley();
                    Toast.makeText(getApplicationContext(), "Registration Successfull", Toast.LENGTH_LONG).show();
                } else {
                    CommonUtils.showToastAlert(SignUpActivityCheff.this, "connection error");
                }
            }
        });
    }
}
