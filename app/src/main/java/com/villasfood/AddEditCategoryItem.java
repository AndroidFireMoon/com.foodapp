package com.villasfood;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.SimpleHTTPConnection;
import com.constants.Utils;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by firemoon on 3/5/17.
 */

public class AddEditCategoryItem extends BaseActivity {

    de.hdodenhof.circleimageview.CircleImageView food_image;
    TextView save_item;
    EditText item_name,price_item,special_text;
    LinearLayout topLayout;
    String pathImage=null;
    CheckBox add_special;
    boolean flagType=false;
    public static final int GALLERY_INTENT_CALLED = 2224;
    public static final int GALLERY_KITKAT_INTENT_CALLED = 500;
    public static final int CUSTOM_REQUEST_CODE = 390;

    public boolean validation(){

        if(TextUtils.isEmpty(item_name.getText().toString().trim())){
            CommonUtils.showToast(context,"Please enter item title");
            return false;
        }else if(TextUtils.isEmpty(price_item.getText().toString().trim())){
            CommonUtils.showToast(context,"Please enter item price");
            return false;
        }else if(pathImage==null){
            CommonUtils.showToast(context,"Please upload image");
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_food_item);
        setHeaderValue(getIntent().getStringExtra("heading"));
        drawerInit();
        back();

        save_item= (TextView) findViewById(R.id.save_item);
        price_item= (EditText) findViewById(R.id.price_item);
        item_name= (EditText) findViewById(R.id.item_name);
        topLayout= (LinearLayout) findViewById(R.id.topLayout);
        food_image= (CircleImageView) findViewById(R.id.food_image);
        add_special= (CheckBox) findViewById(R.id.tick_box_text);
        special_text= (EditText) findViewById(R.id.special_msg);


        // for the case of edit

        if(getIntent().getStringExtra("heading").equalsIgnoreCase("Edit Item")){


            add_special.setVisibility(View.GONE);
            item_name.setText(CommonUtils.getPreferencesString(context,"item_title"));
            price_item.setText(CommonUtils.getPreferencesString(context,"item_price"));

            UrlImageViewHelper.setUrlDrawable(food_image, Utils.encodeURL(CommonUtils.getPreferencesString(context,"item_img")),
                    R.mipmap.circleplaceholder, new UrlImageViewCallback() {
                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {

                            if (!loadedFromCache) {
                                ScaleAnimation scale = new ScaleAnimation(0, 1,
                                        0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                        .5f);
                                scale.setDuration(300);
                                scale.setInterpolator(new OvershootInterpolator());
                                imageView.startAnimation(scale);
                            }

                        }
                    });

        }


        save_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                      if(getIntent().getStringExtra("heading").equalsIgnoreCase("Add Item")){

                          if(validation()) {
                              if (CommonUtils.isNetworkConnected(context)){
                                  if(flagType==true){
                                      if(!TextUtils.isEmpty(special_text.getText().toString().trim())){
                                          new AddItemData(context).execute();
                                      }else{
                                          CommonUtils.showToast(context,"Please give short description for special");
                                      }
                                  }else{
                                      new AddItemData(context).execute();
                                  }
                              }
                              else
                                  CommonUtils.showToast(context, "Please check network connection");
                          }


                      }else{

                          if(CommonUtils.isNetworkConnected(context))
                              new EditCategoryData(context).execute();
                          else
                              CommonUtils.showToast(context,"Please check network connection");

                  }

            }
        });
        topLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialogForChoose();
            }
        });




        add_special.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(add_special.isChecked()){
                    flagType=true;
                    special_text.setVisibility(View.VISIBLE);
                }else{
                    flagType=true;
                    special_text.setVisibility(View.GONE);
                }

            }
        });



    }


    // add/ edit item


    // gall n camera
    protected void ShowDialogForChoose() {

        ArrayAdapter<String> adapt = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, new String[]{
                "Open Camera", "Choose from Gallery"}) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view
                        .findViewById(android.R.id.text1);
                textView.setTextColor(Color.BLACK);
                return view;
            }
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(AddEditCategoryItem.this)
                .setAdapter(
                        new ArrayAdapter<String>(context,
                                android.R.layout.simple_list_item_1,
                                new String[]{"Open Camera",
                                        "From Photo Gallery", "Cancel"}) {
                            public View getView(int position, View convertView,
                                                ViewGroup parent) {
                                View view = super.getView(position,
                                        convertView, parent);
                                TextView textView = (TextView) view
                                        .findViewById(android.R.id.text1);
                                textView.setTextColor(Color.BLACK);
                                return view;
                            }
                        }, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (which == 0) {
                                    Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intentPicture, CUSTOM_REQUEST_CODE);
                                } else if (which == 1) {
                                    if (Build.VERSION.SDK_INT < 19) {
                                        Intent intent = new Intent();
                                        intent.setType("image/jpeg");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"),GALLERY_INTENT_CALLED);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                                        intent.setType("image/jpeg");
                                        startActivityForResult(intent,GALLERY_KITKAT_INTENT_CALLED);
                                    }
                                } else {
                                    dialog.dismiss();
                                }
                            }
                        });
        builder.create().show();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Intent dataVal = data;
            // Bitmap photo = (Bitmap) data.getExtras().get("data");
            // profilepic_addproduct.setImageBitmap(photo);

            if (requestCode == CUSTOM_REQUEST_CODE) {
                if (data!= null && data.getData()!=null ) {
                    pathImage = getPath(this, data.getData());
                    pathImage = compressImage(pathImage);

                    File imgFile = new File(pathImage);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                                .getAbsolutePath());
                        food_image.setImageBitmap(myBitmap);
                    }

                } else {
                    Bitmap bitmap = dataVal.getExtras().getParcelable("data");
                    FileOutputStream out = null;
                    try {

                        String imagename = "img" + requestCode + ".jpg";
                        out = new FileOutputStream(new File(
                                Environment.getExternalStorageDirectory() + "/"
                                        + imagename));
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

                        pathImage = Environment.getExternalStorageDirectory() + "/"
                                + imagename;
                        pathImage = compressImage(pathImage);

                        File imgFile = new File(pathImage);

                        if (imgFile.exists()) {
                            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                                    .getAbsolutePath());
                            food_image.setImageBitmap(myBitmap);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }

            } else if (data != null && requestCode == GALLERY_INTENT_CALLED
                    || requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                if (resultCode != Activity.RESULT_OK)
                    return;
                if (null == data)
                    return;
                Uri originalUri = null;
                if (requestCode == GALLERY_INTENT_CALLED) {
                    originalUri = data.getData();
                } else if (requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                    originalUri = data.getData();
                    final int takeFlags = data.getFlags()
                            & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    // Check for the freshest data.
                    getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
                }
                pathImage = getPath(this, originalUri);
                // path=compressImage(path + "");

//                Utils.write("check path===" + path);
                try {
                    pathImage = compressImage(pathImage);
                    File imgFile = new File(pathImage);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                                .getAbsolutePath());
                        food_image.setImageBitmap(myBitmap);
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String compressImage(String imageUri) {

        String filePath1 = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // by setting this field as true, the actual bitmap pixels are not
        // loaded in the memory. Just the bounds are loaded. If
        // you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath1, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        // max Height and width values of the compressed image is taken as
        // 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the
        // image

        System.out.println("...............1  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                System.out.println("a.................");
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                System.out.println("a.................");
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                System.out.println("c.................");
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        System.out.println("...............2  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);
        // setting inSampleSize value allows to load a scaled down version of
        // the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth,
                actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low
        // on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath1, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath1);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            // write the compressed bitmap at the destination specified by
            // filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("...............3  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".jpg");
        System.out.println("uriSting-----------------" + uriSting);
        return uriSting;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null,
                null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options,
                                     int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }



    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressLint("NewApi")
    public String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getDataColumn(Context context, Uri uri, String selection,
                                String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }



    public ProgressDialog dialog;

    public class AddItemData extends
            AsyncTask<Void, Integer, String> {
        String datacheck;
        ArrayList<ByteArrayBody> images=new ArrayList<>();
        Context cont;


        public AddItemData(Activity context) {
            cont=context;
            dialog = new ProgressDialog(cont);

        }

        @Override
        protected String doInBackground(Void... params) {


            MultipartEntity mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            HttpPost httppost = new HttpPost(AppConstant.ADD_ITEM);

            try {
                mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                if(pathImage!=null){
                    //    File file = new File(pathImage);

                    Bitmap  bm = BitmapFactory.decodeFile(pathImage);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                    byte[] data = bos.toByteArray();
                    ByteArrayBody bab = new ByteArrayBody(data, "forest.jpg");

                    mp.addPart("image",bab);
                    mp.addPart("item_title", new StringBody(item_name.getText().toString().trim(), "text/plain", Charset.forName("UTF-8")));
                    mp.addPart("item_price", new StringBody(price_item.getText().toString().trim(), "text/plain", Charset.forName("UTF-8")));
                    mp.addPart("cat_id", new StringBody(CommonUtils.getPreferencesString(context,"cat_id"), "text/plain", Charset.forName("UTF-8")));
                    if(flagType==true)
                    mp.addPart("special", new StringBody(special_text.getText().toString().trim(), "text/plain", Charset.forName("UTF-8")));

                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            httppost.setEntity(mp);
            return SimpleHTTPConnection.sendByPOST(httppost);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setCancelable(false);
            dialog.setMessage("Sending data to admin..");
            dialog.show();

        }

        @Override
        protected void onPostExecute(String result) {

            Utils.write("result        "+result);

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (result != null) {
                try {
                    JSONObject object=new JSONObject(result);
                    JSONArray array=object.getJSONArray("data");

                    CommonUtils.showToast(context,"Category Added Successfully");
                    finish();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }
    }



    public class EditCategoryData extends
            AsyncTask<Void, Integer, String> {
        String datacheck;
        ArrayList<ByteArrayBody> images=new ArrayList<>();
        Context cont;


        public EditCategoryData(Activity context) {
            cont=context;
            dialog = new ProgressDialog(cont);

        }

        @Override
        protected String doInBackground(Void... params) {


            MultipartEntity mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            HttpPost httppost = new HttpPost(AppConstant.EDIT_ITEM);

            try {
                mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);


                    //    File file = new File(pathImage);


                if(pathImage!=null){
                    Bitmap  bm = BitmapFactory.decodeFile(pathImage);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                    byte[] data = bos.toByteArray();
                    ByteArrayBody bab = new ByteArrayBody(data, "forest.jpg");
                    mp.addPart("image",bab);
                }

                    mp.addPart("item_title", new StringBody(item_name.getText().toString().trim(), "text/plain", Charset.forName("UTF-8")));
                    mp.addPart("item_price", new StringBody(price_item.getText().toString().trim(), "text/plain", Charset.forName("UTF-8")));
                    mp.addPart("item_id", new StringBody(CommonUtils.getPreferencesString(context,"item_id"), "text/plain", Charset.forName("UTF-8")));



            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            httppost.setEntity(mp);
            return SimpleHTTPConnection.sendByPOST(httppost);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setCancelable(false);
            dialog.setMessage("Sending data to admin..");
            dialog.show();

        }

        @Override
        protected void onPostExecute(String result) {

            System.out.println("resulttt           "+result);

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (result != null) {
                try {
                    JSONObject object=new JSONObject(result);
                    JSONArray array=object.getJSONArray("data");

                    CommonUtils.showToast(context,"Category Updated Successfully");
                    finish();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }
    }

}
