package com.villasfood;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;

import android.widget.Toast;

/**
 * Created by firemoon on 20/3/17.
 */

public class LoginUser extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    public String check;
    EditText user_name_login;
    Button login_button_user;
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    String array;
    String a;


    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void AsynctaskResult(String arr, String action,
                                HashMap<String, Object> webservices_params, boolean status) {
        try{            array=arr;


            if(action.equalsIgnoreCase("http://www.avalon.villas/foodapp/v1/loginnewuser")){

                System.out.println("arr                 "+arr);

                check="success";
                CommonUtils.savePreferencesString(context,"check",check);

                JSONObject object=new JSONObject(arr);
                if(object.getString("error").equalsIgnoreCase("false")){

                    Utils.write("printuserid      "+object.getJSONArray("data").getJSONObject(0).getString("user_id"));

                    CommonUtils.savePreferencesString(context,"user_local_id",object.getJSONArray("data").getJSONObject(0).getString("user_id"));
                    CommonUtils.savePreferencesString(context,"username",object.getJSONArray("data").getJSONObject(0).getString("username"));
                    CommonUtils.savePreferencesString(context,"api_key",object.getJSONArray("data").getJSONObject(0).getString("api_key"));

                //    CommonUtils.savePreferencesString(context,"user_img",object.getJSONArray("data").getJSONObject(0).getString("user_img"));


                    System.out.println("arr1       "+CommonUtils.getPreferencesString(context,"user_local_id"));


                    if (ActivityCompat.checkSelfPermission(LoginUser.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(LoginUser.this, new String[]{Manifest.permission.INTERNET,Manifest.permission.ACCESS_NETWORK_STATE}, 200);
                    }else{
                        CommonUtils.savePreferencesString(context,"data",arr);
                        startActivity(new Intent(LoginUser.this, MainActivity.class).putExtra("from","normal"));
                        Toast.makeText(getApplicationContext(),"Login successful", Toast.LENGTH_LONG).show();
                         finish();
                    }


                }else if (object.getString("error").equalsIgnoreCase("true"))
                {
                    Toast.makeText(getApplicationContext(),object.getString("message"), Toast.LENGTH_LONG).show();
                }
                else
                    CommonUtils.showToast(context,object.getString("Message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case 200:
                CommonUtils.savePreferencesString(context,"data",array);
                startActivity(new Intent(LoginUser.this, MainActivity.class).putExtra("from","normal"));
                finish();
                break;
        }
    }


    protected void makeVolley(){
        Utils.write("id     "+AppConstant.token );
        callBackListner = LoginUser.this;
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("username", user_name_login.getText().toString().trim());
        params.put("device_id", AppConstant.token);
        params.put("is_user","1");

        Utils.write(user_name_login.getText().toString().trim()+"      token     "+AppConstant.token);

        new AsyncTaskJSONObject(true, params, callBackListner, context, "http://www.avalon.villas/foodapp/v1/loginnewuser");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_user);
        a= getDeviceId(this);
        clicking();
        CommonUtils.savePreferencesString(context,"cart",String.valueOf(0));
    }

    private void clicking() {
        login_button_user= (Button)findViewById(R.id.login_button_user);
        user_name_login= (EditText) findViewById(R.id.user_name_login);


        user_name_login.setTypeface(Utils.setFontText(LoginUser.this));
        login_button_user.setTypeface(Utils.setFontText(LoginUser.this));


        login_button_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.write("before token    "+AppConstant.token);
                    AppConstant.token = FirebaseInstanceId.getInstance().getToken();

                Utils.write("token    "+AppConstant.token);

                    if (CommonUtils.isNetworkConnected(LoginUser.this)) {
                        makeVolley();
                    } else {
                        CommonUtils.showToastAlert(LoginUser.this, "connection error");
                    }



            }
        });
        //Utils.showToastS(context,"Work in progress");
    }
}
