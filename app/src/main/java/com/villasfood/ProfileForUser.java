package com.villasfood;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.SessionManagement;
import com.constants.SimpleHTTPConnection;
import com.constants.Utils;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class ProfileForUser extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    // new var
    SessionManagement sessionManagement;
    EditText textView,email,address,password,mobile;
    String text, textemail;
    RelativeLayout coverPage;
    ByteArrayBody bab;
    String pathImage="";
    Button save_btn;
    public static final int GALLERY_INTENT_CALLED = 223;
    public static final int GALLERY_KITKAT_INTENT_CALLED = 500;
    public static final int CUSTOM_REQUEST_CODE = 39400;
    ImageView header_cover_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        back();
        callBackListner=this;
        drawerInit();
        setHeaderValue("Profile");
        sessionManagement= new SessionManagement(getApplicationContext());
        text= ""+ sessionManagement.getUserDetails().get(SessionManagement.KEY_NAME);
        textemail= ""+ sessionManagement.getUserDetails().get(SessionManagement.KEY_EMAIL);
        coverPage= (RelativeLayout) findViewById(R.id.coverPage);
        textView= (EditText)findViewById(R.id.username);
        email= (EditText)findViewById(R.id.email);
        address= (EditText)findViewById(R.id.address);
        password= (EditText)findViewById(R.id.password);
        mobile= (EditText)findViewById(R.id.mobile);
        header_cover_image= (ImageView) findViewById(R.id.header_cover_image);

        save_btn= (Button) findViewById(R.id.saveProfile);
        coverPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                CommonUtils.hideKeyboard(context);
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeVolley();

            }
        });
        textView.setText(CommonUtils.getPreferencesString(context,"username"));

        coverPage.setBackgroundResource(R.mipmap.distort);


        header_cover_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null ) {
                    if (ActivityCompat.checkSelfPermission(ProfileForUser.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ProfileForUser.this, new String[]{android.Manifest.permission.INTERNET, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                    }else{

                        ShowDialogForChoose(context);
                    }

                }else{
                    ShowDialogForChoose(context);
                }
            }
        });

    //   Utils.hideSoftKeyboard(context);


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    ShowDialogForChoose(context);

                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        textView.setEnabled(false);
        mobile.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        address.setVisibility(View.GONE);
        password.setVisibility(View.GONE);
        save_btn.setVisibility(View.GONE);

        makeVolley();

      /*  UrlImageViewHelper.setUrlDrawable(header_cover_image, Utils.encodeURL(CommonUtils.getPreferencesString(context,"user_img")),
                0, new UrlImageViewCallback() {
                    @Override
                    public void onLoaded(ImageView imageView,
                                         Bitmap loadedBitmap, String url,
                                         boolean loadedFromCache) {

                        if (!loadedFromCache) {
                            ScaleAnimation scale = new ScaleAnimation(0, 1,
                                    0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                    .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                    .5f);
                            scale.setDuration(300);
                            scale.setInterpolator(new OvershootInterpolator());
                            imageView.startAnimation(scale);
                        }
                    }
                });*/

    }

    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    @Override
    public void AsynctaskResult(String arr, String action,
                                HashMap<String, Object> webservices_params, boolean status) {
        try{

            System.out.println(arr);

            JSONObject refer=new JSONObject(arr);

            if(refer.getJSONArray("data").getJSONObject(0).getString("user_img").equalsIgnoreCase("")){
                header_cover_image.setImageResource(R.mipmap.appcon);
            }else {


                UrlImageViewHelper.setUrlDrawable(header_cover_image, Utils.encodeURL(refer.getJSONArray("data").getJSONObject(0).getString("user_img")),
                        0, new UrlImageViewCallback() {
                            @Override
                            public void onLoaded(ImageView imageView,
                                                 Bitmap loadedBitmap, String url,
                                                 boolean loadedFromCache) {

                                if (!loadedFromCache) {
                                    ScaleAnimation scale = new ScaleAnimation(0, 1,
                                            0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f);
                                    scale.setDuration(300);
                                    scale.setInterpolator(new OvershootInterpolator());
                                    imageView.startAnimation(scale);
                                }
                            }
                        });

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void makeVolley(){

        Utils.write("userid         "+CommonUtils.getPreferencesString(context,"user_local_id"));
        HashMap<String, Object> params = new LinkedHashMap<>();

        params.put("user_id", CommonUtils.getPreferencesString(context,"user_local_id"));
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.USER_IMAAGE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                //  logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // mp



    public void ShowDialogForChoose(Activity context) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setAdapter(
                        new ArrayAdapter<String>(context,
                                android.R.layout.simple_list_item_1,
                                new String[]{
                                        "Open Camera","From Photo Gallery", "Cancel"}) {
                            public View getView(int position, View convertView,
                                                ViewGroup parent) {
                                View view = super.getView(position,
                                        convertView, parent);
                                TextView textView = (TextView) view
                                        .findViewById(android.R.id.text1);
                                textView.setTextColor(Color.BLACK);
                                return view;
                            }
                        }, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (which == 0) {
                                    Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intentPicture, CUSTOM_REQUEST_CODE);

                                }
                                else if (which == 1) {
                                    if (Build.VERSION.SDK_INT > 19) {
                                        Intent intent = new Intent();
                                        intent.setType("image/jpeg");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"), GALLERY_INTENT_CALLED);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                                        intent.setType("image/jpeg");
                                        startActivityForResult(intent, GALLERY_KITKAT_INTENT_CALLED);
                                    }
                                } else {
                                    dialog.dismiss();
                                }
                            }
                        });

        builder.create().show();



    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Intent dataVal = data;
            // Bitmap photo = (Bitmap) data.getExtras().get("data");
            // profilepic_addproduct.setImageBitmap(photo);

            if (requestCode == CUSTOM_REQUEST_CODE) {
                if (data.getData() != null) {
                    pathImage = getPath(context, data.getData());
                    pathImage = compressImage(pathImage);

                    File imgFile = new File(pathImage);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());




                        header_cover_image.setImageBitmap(myBitmap);
                    }

                } else {
                    Bitmap bitmap = dataVal.getExtras().getParcelable("data");
                    FileOutputStream out = null;
                    try {

                        String imagename = "img" + requestCode + ".jpg";
                        out = new FileOutputStream(new File(
                                Environment.getExternalStorageDirectory() + "/"
                                        + imagename));
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

                        pathImage = Environment.getExternalStorageDirectory() + "/"
                                + imagename;
                        pathImage = compressImage(pathImage);

                        File imgFile = new File(pathImage);

                        if (imgFile.exists()) {
                            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                                    .getAbsolutePath());



                            header_cover_image.setImageBitmap(myBitmap);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }

            } else if (data != null && requestCode == GALLERY_INTENT_CALLED
                    || requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                if (resultCode != Activity.RESULT_OK)
                    return;
                if (null == data)
                    return;
                Uri originalUri = null;
                if (requestCode == GALLERY_INTENT_CALLED) {
                    originalUri = data.getData();
                } else if (requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                    originalUri = data.getData();
                    final int takeFlags = data.getFlags()
                            & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    // Check for the freshest data.
                    context.getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
                }
                pathImage = getPath(context, originalUri);
                // path=compressImage(path + "");

//                Utils.write("check path===" + path);
                try {
                    pathImage = compressImage(pathImage);

                    File imgFile = new File(pathImage);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                                .getAbsolutePath());
                        header_cover_image.setImageBitmap(myBitmap);


                        /*if(typeOfPic.equalsIgnoreCase("image1")){
                            path1=pathImage;
                            image1.setImageBitmap(myBitmap);
                        }else if(typeOfPic.equalsIgnoreCase("image2")){
                            path2=pathImage;
                            image2.setImageBitmap(myBitmap);
                        }else{

                            path3=pathImage;
                            image3.setImageBitmap(myBitmap);
                        }*/


                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }


            Utils.write("pathImage       "+pathImage);

            if(pathImage!=null)
            new SubmitDataToServer("",context).execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public String compressImage(String imageUri) {

        String filePath1 = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // by setting this field as true, the actual bitmap pixels are not
        // loaded in the memory. Just the bounds are loaded. If
        // you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath1, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        // max Height and width values of the compressed image is taken as
        // 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the
        // image

        System.out.println("...............1  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                System.out.println("a.................");
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                System.out.println("a.................");
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                System.out.println("c.................");
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        System.out.println("...............2  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);
        // setting inSampleSize value allows to load a scaled down version of
        // the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth,
                actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low
        // on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath1, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath1);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            // write the compressed bitmap at the destination specified by
            // filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("...............3  " + "actualHeight:"
                + actualHeight + "actualWidth:" + actualWidth + "maxHeight:"
                + maxHeight + "maxWidth:" + maxWidth + "imgRatio:" + imgRatio
                + "maxRatio:" + maxRatio);
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".jpg");
        System.out.println("uriSting-----------------" + uriSting);
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null,
                null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options,
                                     int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open("WineryApp.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressLint("NewApi")
    public String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getDataColumn(Context context, Uri uri, String selection,
                                String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }






    public ProgressDialog dialog;
    public class SubmitDataToServer extends
            AsyncTask<Void, Integer, String> {
        String datacheck;
        Context cont;


        public SubmitDataToServer(String ctx, Activity context) {
            datacheck = ctx;
            cont=context;
            dialog = new ProgressDialog(cont);

        }

        @Override
        protected String doInBackground(Void... params) {


            MultipartEntity mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            HttpPost httppost = new HttpPost(AppConstant.USER_PROFILE);

            try {
                mp = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);


                Bitmap bm = BitmapFactory.decodeFile(pathImage);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                byte[] data = bos.toByteArray();
                bab = new ByteArrayBody(data, "forest.jpg");
                mp.addPart("image",bab);
                System.out.println("sop file======================="+bab);



               /* if(pathImage!=null){
                    File file = new File(pathImage);
                    Log.d("EDIT USER PROFILE", "UPLOAD: file length = " + file.length());
                    Log.d("EDIT USER PROFILE", "UPLOAD: file exist = " + file.exists());
                    mp.addPart("image", new FileBody(file, "application/octet"));

                    Utils.write("entry inside file==================="+new FileBody(file, "application/octet"));
                    System.out.println("sop file=======================");
                }*/

                mp.addPart("user_id",new StringBody(CommonUtils.getPreferencesString(context,"user_local_id").trim(),
                        "text/plain", Charset.forName("UTF-8")));






                Utils.write("pathImage           "+pathImage);
                Utils.write("pathImage           "+CommonUtils.getPreferencesString(context,"user_local_id"));




            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            httppost.setEntity(mp);
            return SimpleHTTPConnection.sendByPOST(httppost);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setCancelable(false);
            dialog.setMessage("Sending data to admin..");
            dialog.show();

        }

        @Override
        protected void onPostExecute(String result) {

            System.out.println("UPDATE USER PROFILE VALUES===" + result);
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (result != null) {
                try {
                    JSONObject object=new JSONObject(result);

                    if(object.getString("error").equalsIgnoreCase("false")) {
                        CommonUtils.showToast(context, "Submitted successfully");

                        JSONObject obj = object.getJSONArray("data").getJSONObject(0);

                        CommonUtils.savePreferencesString(context,"user_img",obj.getString("user_img"));


                        UrlImageViewHelper.setUrlDrawable(header_cover_image, Utils.encodeURL(obj.getString("user_img")),
                                0, new UrlImageViewCallback() {
                                    @Override
                                    public void onLoaded(ImageView imageView,
                                                         Bitmap loadedBitmap, String url,
                                                         boolean loadedFromCache) {

                                        if (!loadedFromCache) {
                                            ScaleAnimation scale = new ScaleAnimation(0, 1,
                                                    0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                                    .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                                    .5f);
                                            scale.setDuration(300);
                                            scale.setInterpolator(new OvershootInterpolator());
                                            imageView.startAnimation(scale);
                                        }
                                    }
                                });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }
    }

}
