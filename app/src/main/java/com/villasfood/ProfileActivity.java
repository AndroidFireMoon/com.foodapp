package com.villasfood;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.SessionManagement;
import com.constants.Utils;

import org.apache.http.entity.mime.content.ByteArrayBody;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ProfileActivity extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    SessionManagement sessionManagement;
    EditText textView,email,address,password,mobile;
    String text, textemail;
    RelativeLayout coverPage;
    ByteArrayBody bab;
    String pathImage="";
    Button save_btn;
    public static final int GALLERY_INTENT_CALLED = 223424;
    public static final int GALLERY_KITKAT_INTENT_CALLED = 55000;
    public static final int CUSTOM_REQUEST_CODE = 39400;
    ImageView header_cover_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
         back();
        callBackListner=this;
        drawerInit();
        setHeaderValue("Profile");

        sessionManagement= new SessionManagement(getApplicationContext());
        text= ""+ sessionManagement.getUserDetails().get(SessionManagement.KEY_NAME);
        textemail= ""+ sessionManagement.getUserDetails().get(SessionManagement.KEY_EMAIL);

        coverPage= (RelativeLayout) findViewById(R.id.coverPage);
        textView= (EditText)findViewById(R.id.username);
        email= (EditText)findViewById(R.id.email);
        address= (EditText)findViewById(R.id.address);
        password= (EditText)findViewById(R.id.password);
        mobile= (EditText)findViewById(R.id.mobile);
        header_cover_image= (ImageView)findViewById(R.id.header_cover_image);

        save_btn= (Button) findViewById(R.id.saveProfile);
        coverPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.hideKeyboard(context);
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeVolley();

            }
        });

        textView.setText(CommonUtils.getPreferencesString(context,"firstname"));
        email.setText(CommonUtils.getPreferencesString(context,"email"));
    }

    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    @Override
    public void AsynctaskResult(String arr, String action,
                                HashMap<String, Object> webservices_params, boolean status) {
        try{
            System.out.println(arr);
         Utils.showToastS(context,"Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void makeVolley(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id", CommonUtils.getPreferencesString(context,"user_id"));
        params.put("username", textView.getText().toString().trim());
        params.put("mobile", mobile.getText().toString().trim());
        params.put("address", address.getText().toString().trim());
        params.put("email", email.getText().toString().trim());
        params.put("password", password.getText().toString().trim());
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.UPDATE_PROFILE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                //  logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    // profile




}
