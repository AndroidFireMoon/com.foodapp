package com.villasfood;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.adapter.UserAdapterCheff;
import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 3/5/17.
 */

public class UserOrders extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner
{

    ArrayList<JSONObject> objectDatabase = new ArrayList<>();
    ListView listOrders;
    HashMap<String, ArrayList<JSONObject>> checkHashMap=new HashMap<>();
    ArrayList<JSONObject> dataString=new ArrayList<>();
    ArrayList<String> sortedString=new ArrayList<>();
    Double totalAmount;
    ArrayList<Double> referList=new ArrayList<>();
    UserAdapterCheff referenceAdapter;
    String admin_id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_detail_layout);
        context=this;
        callBackListner=this;
        back();
        drawerInit();
        setHeaderValue("Order By");
        listOrders= (ListView) findViewById(R.id.list_data);
        admin_id=CommonUtils.getPreferencesString(context,"user_local_id");
     //   Utils.write("userid            "+admin_id);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonUtils.isNetworkConnected(context))
            userListOrder();
        else
            Utils.showToastS(context,"Network-Error");
    }

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {
        try{
            System.out.println("entry is not available userlist    "+arr);
            System.out.println("abc     "+arr);



            if(action.equalsIgnoreCase(AppConstant.USER_LIST)){
                objectDatabase.clear();
                JSONObject object=new JSONObject(arr);

                if(object.getString("error").equalsIgnoreCase("false")) {
                JSONArray data=object.getJSONArray("data");
                for(int j=0;j<data.length();j++) {
                    objectDatabase.add(data.getJSONObject(j));
                }
                    referenceAdapter=new UserAdapterCheff(context,objectDatabase,callBackListner);
                    listOrders.setAdapter(referenceAdapter);
                }else{

                    CommonUtils.showToast(context,object.getString("message"));
                    listOrders.setAdapter(null);
                    finish();
                }

                listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            CommonUtils.savePreferencesString(context,"user_list_id",objectDatabase.get(position).getString("user_id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(new Intent(context,OrderSeperation.class));
                    }
                });
            }else if(action.equalsIgnoreCase(AppConstant.PAID_BILLS)){
                System.out.println("entry 1");
            }else if(action.equalsIgnoreCase(AppConstant.DELETE_USER)){
                userListOrder();
            }else if(action.equalsIgnoreCase(AppConstant.SEPERATE_ORDER)){

                    checkHashMap.clear();
                    dataString.clear();
                    sortedString.clear();
                    referList.clear();

                    String var = "";
                    JSONObject objectData = new JSONObject(arr);
                    if(objectData.getString("error").equalsIgnoreCase("false"))
                    {

                        JSONArray dataArray = objectData.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            var = dataArray.getJSONObject(i).getString("rank");

                            if (!sortedString.contains(var)) {
                                sortedString.add(var);
                            }
                            dataString.add(dataArray.getJSONObject(i));

                            totalAmount=Double.parseDouble(dataArray.getJSONObject(i).getString("price"));
                            referList.add(totalAmount);


                        }

                        for (int k = 0; k < sortedString.size(); k++) {
                            ArrayList<JSONObject> temp = new ArrayList<>();

                            for (int j = 0; j < dataString.size(); j++) {
                                if (sortedString.get(k).equalsIgnoreCase(dataString.get(j).getString("rank"))) {
                                    temp.add(dataString.get(j));
                                }
                            }
                            checkHashMap.put(sortedString.get(k), temp);
                        }
//                        CommonUtils.savePreferencesString(context,"totalorder",);
//                        CommonUtils.savePreferencesString(context,"totalamount",);


                        referenceAdapter.emailUser(CommonUtils.getPreferencesString(context,"user_local_id"),String.valueOf(checkHashMap.size()),String.valueOf(sum(referList)),referList);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void userListOrder(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.USER_LIST);
    }


    public Double sum(ArrayList<Double> list) {
        Double sum = 0.00;
        for (Double i : list)
            sum = sum + i;
        return sum;
    }



}
