package com.villasfood;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;
import com.urlimageviewhelper.UrlImageViewCallback;
import com.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by firemoon on 29/4/17.
 */

public class MenuCorousel extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    ListView menu_item;
    ArrayList<String> food_item=new ArrayList<>();
    ArrayList<TextView> view_item=new ArrayList<>();
    int positionScroller = 0;
    Gallery gallery;
    View pView;
    ImageView left,right,cart,search;
    EditText et_search_offer;
    boolean isShowing=true;
    MenuCorouselAdapter myAdapter;
    int size;
    ArrayList<JSONObject> datalist;
    ArrayList<JSONObject> plantData=new ArrayList<>();
    ArrayList<String> nameSearch=new ArrayList<>();
    LinearLayout parentLayout;
    TextView textOne;
    RelativeLayout addCartLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            setContentView(R.layout.mennu_food_item);
            setHeaderValue("Menu Items");
            drawerInit();
            back();
            callBackListner=this;
            cart= (ImageView) findViewById(R.id.addCart);
            addCartLL= (RelativeLayout) findViewById(R.id.addCartLL);
            et_search_offer= (EditText) findViewById(R.id.et_search_offer);
            search= (ImageView) findViewById(R.id.search);
            left= (ImageView) findViewById(R.id.leftaaro);
            right= (ImageView) findViewById(R.id.rightarro);
            menu_item= (ListView) findViewById(R.id.list_data);
            textOne= (TextView) findViewById(R.id.textOne);
            parentLayout= (LinearLayout) findViewById(R.id.parentLayout);
            gallery = (Gallery) findViewById(R.id.gallery1);
            gallery.setSpacing(5);
            search.setVisibility(View.GONE);
            addCartLL.setVisibility(View.VISIBLE);

            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hideSoftKeyboard(context);
                }
            });


            menu_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        Utils.hideSoftKeyboard(context);
                        CommonUtils.savePreferencesString(context,"counter",String.valueOf(counter));
                        CommonUtils.savePreferencesString(context,"item_id",objectItem.get(position).getString("item_id"));
                        CommonUtils.savePreferencesString(context,"item_img",objectItem.get(position).getString("item_img"));
                        CommonUtils.savePreferencesString(context,"item_price",objectItem.get(position).getString("item_price"));
                        CommonUtils.savePreferencesString(context,"item_title",objectItem.get(position).getString("item_title"));
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    startActivity(new Intent(MenuCorousel.this,DetailFood.class));
                }
            });

            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MenuCorousel.this,CartScreen.class));
                }
            });

            left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Utils.hideSoftKeyboard(context);
                    et_search_offer.setVisibility(View.GONE);

                    if (positionScroller > 0) {
                        --positionScroller;
                         gallery.setSelection(positionScroller, false);
                        try {
                            getItems(objectDatabase.get(positionScroller).getString("cat_id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

            right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Utils.hideSoftKeyboard(context);
                    et_search_offer.setVisibility(View.GONE);

                    if (positionScroller < size ) {
                        ++positionScroller;
                        gallery.setSelection(positionScroller, false);
                        try {
                            getItems(objectDatabase.get(positionScroller).getString("cat_id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
           /* search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(isShowing==true) {
                        isShowing=false;
                        et_search_offer.setVisibility(View.VISIBLE);
                    }else {
                        isShowing=true;
                        et_search_offer.setVisibility(View.GONE);
                    }
                }
            });
*/
            et_search_offer.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count){
                    System.out.println("Text ["+s+"]");
                    try{
                        myAdapter.getFilter().filter(s.toString());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });



            if (CommonUtils.isNetworkConnected(context))
                makeVolley();
            else
                Utils.showToastS(context,"Network-Error");



        }catch (Exception e){
            e.printStackTrace();
        }

    }
    @Override
    protected void onResume(){
        super.onResume();
        Utils.write("cart      "+CommonUtils.getPreferencesString(context,"cart"));
        textOne.setText(CommonUtils.getPreferencesString(context,"cart"));

    }

    private void corouselData(final int size) {
        try {

            Utils.write("pos         "+getIntent().getStringExtra("position"));

            gallery.setAdapter(new GalleryImageAdapter(context, objectDatabase));
            getItems(objectDatabase.get(Integer.parseInt(getIntent().getStringExtra("position"))).getString("cat_id"));
            gallery.setSelection(Integer.parseInt(getIntent().getStringExtra("position")));
            gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    positionScroller = position;
                    Utils.hideSoftKeyboard(context);
                    et_search_offer.setVisibility(View.GONE);
                    try {
                        getItems(objectDatabase.get(position).getString("cat_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });

            gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        if (pView != null) {
                            pView.setBackgroundColor(Color.parseColor("#d3d3d3"));
                        }
                    view.setBackgroundColor(Color.parseColor("#ffffff"));

                    Utils.write("position      " + position);
                    pView = view;


                    //additional

                    positionScroller = position;

                    if (positionScroller > 0 && positionScroller < size) {

                        left.setVisibility(View.VISIBLE);
                        right.setVisibility(View.VISIBLE);

                    } else if (positionScroller == 0) {

                        left.setVisibility(View.INVISIBLE);

                    } else if (positionScroller == size) {

                        right.setVisibility(View.INVISIBLE);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected class GalleryImageAdapter extends BaseAdapter {
        private Context mContext;

        /*
         * private Integer[] mImagfeIds = { R.drawable.ic_launcher,
         * R.drawable.ic_launcher, R.drawable.image3, R.drawable.ic_launcher,
         * R.drawable.ic_launcher, R.drawable.image6, R.drawable.image7,
         * R.drawable.image8 };
         */
        ArrayList<JSONObject> referral = new ArrayList<JSONObject>();

        public GalleryImageAdapter(Context context, ArrayList<JSONObject> chooseImage) {

            mContext = context;
            referral = chooseImage;

        }

        public int getCount() {
            return referral.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        // Override this method according to your need
        public View getView(int index, View view, ViewGroup viewGroup) {
            // TODO Auto-generated method stub
            i = new TextView(mContext);
            i.setGravity(Gravity.CENTER);
            i.setSingleLine();
            i.setTextSize(15.0f);
            i.setBackgroundColor(Color.parseColor("#d3d3d3"));
            i.setTextColor(Color.parseColor("#000000"));
            i.setLayoutParams(new Gallery.LayoutParams(280, 180));
            try {
                i.setText(referral.get(index).getString("cat_title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            positionScroller = index;

            if(!view_item.contains(i))
                view_item.add(i);

            return i;
        }
    }

    TextView i;
    protected void makeVolley(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.CAT_LIST);
    }

    ArrayList<JSONObject> objectDatabase = new ArrayList<>();
    ArrayList<JSONObject> objectItem = new ArrayList<>();

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {

        try{
            if(action.equalsIgnoreCase(AppConstant.CAT_LIST)){
                objectDatabase.clear();
                JSONObject object=new JSONObject(arr);
                JSONArray data=object.getJSONArray("data");
                for(int j=0;j<data.length();j++)
                    objectDatabase.add(data.getJSONObject(j));

                 size = objectDatabase.size() - 1;

                Utils.write("chek     "+size);

                 corouselData(size);
            }
            if(action.equalsIgnoreCase(AppConstant.LIST_ITEM)){
                nameSearch.clear();
                objectItem.clear();
                JSONObject object=new JSONObject(arr);
                JSONArray data=object.getJSONArray("data");

                for(int j=0;j<data.length();j++) {
                    objectItem.add(data.getJSONObject(j));
                    nameSearch.add(data.getJSONObject(j).getString("item_title"));
                }

                myAdapter = new MenuCorouselAdapter(context,objectItem,nameSearch);
                menu_item.setAdapter(myAdapter);
//                menu_item.setAdapter(new MenuCorouselAdapter(context,objectItem,nameSearch));
            }
            if(action.equalsIgnoreCase(AppConstant.ADD_CART)){

                    JSONObject object=new JSONObject(arr);
                    int sizeCart =object.getJSONArray("data").length();
                    CommonUtils.savePreferencesString(context,"cart",String.valueOf(sizeCart));

                    textOne.setText(""+sizeCart);

                if(object.getString("error").equalsIgnoreCase("false")){
                    Utils.showToastS(context,"Item added to cart successfully");
                }


                     Utils.write("data     "+arr);

            }
        }catch (Exception e){
            e.printStackTrace();
          //  menu_item.setAdapter(new MenuCorouselAdapter(context,objectItem, nameSearch));
        }
    }

    protected void getItems(String cat_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("cat_id",cat_id);
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.LIST_ITEM);
    }




    // adapter <code></code>

   int netWorth;
    /**
     * Created by firemoon on 10/5/17.
     */
    public static int counter=1;
    boolean increment=false;

    int money;
    public class MenuCorouselAdapter extends BaseAdapter implements Filterable {
        String stringVal;

        private ItemFilter mFilter = new ItemFilter();
        private List<String> originalData = null;
        private List<String>filteredData = null;
        private LayoutInflater mInflater;
        Activity ctx;
        ArrayList<JSONObject> plantData;
        ArrayList<String> subplantData;
        public MenuCorouselAdapter(Activity context, ArrayList<JSONObject> datalist, ArrayList<String> nameSearch){
            ctx=context;
            this.plantData=datalist;
            subplantData=nameSearch;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return subplantData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return subplantData.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            final TextView tv1, tv2, tv3,tv4,tv5;

            de.hdodenhof.circleimageview.CircleImageView food_icon;
            ImageView decrease,increase;
            LayoutInflater inflater = ctx.getLayoutInflater();
            View row1 = inflater.inflate(R.layout.menu_item_recycle, parent, false);
            tv1 = (TextView) row1.findViewById(R.id.recipie_name);
            tv2 = (TextView) row1.findViewById(R.id.recipie_price);
            tv3 = (TextView) row1.findViewById(R.id.quantity);
            tv4 = (TextView) row1.findViewById(R.id.special_des);
            tv5 = (TextView) row1.findViewById(R.id.atc_list);
            decrease = (ImageView) row1.findViewById(R.id.decrease);
            increase = (ImageView) row1.findViewById(R.id.increase);
            food_icon = (de.hdodenhof.circleimageview.CircleImageView) row1.findViewById(R.id.food_icon);


            try {

                tv4.setText(plantData.get(position).getString("special"));

                tv1.setText(subplantData.get(position));
                tv2.setText("RP  "+plantData.get(position).getString("item_price"));

                decrease.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        increment=true;

                        if(counter>1){
                            counter = Integer.parseInt(tv3.getText().toString());
                            counter-=1;
                            stringVal = Integer.toString(counter);
                            tv3.setText(stringVal);

                            try {
                                counter = Integer.parseInt(tv3.getText().toString());
                                int money=Integer.parseInt(plantData.get(position).getString("item_price"));
                                netWorth = money * counter;
                                tv2.setText("USD "+netWorth);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });

                increase.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        increment=true;

                        counter = Integer.parseInt(tv3.getText().toString());
                        counter+=1;
                        stringVal = Integer.toString(counter);
                        tv3.setText(stringVal);

                        try {
                             money=Integer.parseInt(plantData.get(position).getString("item_price"));
                             netWorth = money * counter;
                            tv2.setText("USD "+netWorth);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


                tv5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Utils.write("netWorth    "+netWorth);
                            Utils.write("counter    "+counter);

                            if(increment==true){
                                addCart(netWorth,counter,plantData.get(position).getString("item_id"));
                            }else
                            {
                                int money=Integer.parseInt(plantData.get(position).getString("item_price"));
                                netWorth = money * counter;
                                addCart(netWorth,counter,plantData.get(position).getString("item_id"));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                UrlImageViewHelper.setUrlDrawable(food_icon, Utils.encodeURL(plantData.get(position).getString("item_img")),
                        R.mipmap.foodplaceholder, new UrlImageViewCallback() {
                            @Override
                            public void onLoaded(ImageView imageView,
                                                 Bitmap loadedBitmap, String url,
                                                 boolean loadedFromCache) {

                                if (!loadedFromCache) {
                                    ScaleAnimation scale = new ScaleAnimation(0, 1,
                                            0, 1, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f, ScaleAnimation.RELATIVE_TO_SELF,
                                            .5f);
                                    scale.setDuration(300);
                                    scale.setInterpolator(new OvershootInterpolator());
                                    imageView.startAnimation(scale);
                                }
                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return row1;
        }

        @Override
        public Filter getFilter() {
            return mFilter;
        }


        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<String> list = nameSearch;

                int count = list.size();
                final ArrayList<String> nlist = new ArrayList<String>(count);
                datalist = new ArrayList<JSONObject>(count);

                String filterableString = null;
                JSONObject value=null;

                for (int i = 0; i < count; i++) {
                    try {
                        filterableString = list.get(i);
                        value= plantData.get(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(filterableString);
                        datalist.add(value);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                subplantData = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }

        }

    }



    protected void addCart(int netWorth, int counter, String item_id){

        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("item_id",item_id);
        params.put("user_id",CommonUtils.getPreferencesString(context,"user_local_id"));
        params.put("price",String.valueOf(netWorth));
        params.put("quantity",String.valueOf(counter));
        params.put("extra_cheese","");
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.ADD_CART);

    }

}


