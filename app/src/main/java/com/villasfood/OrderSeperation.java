package com.villasfood;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.adapter.SeperationAdapter;
import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 4/8/17.
 */


public class OrderSeperation extends BaseActivity implements AsyncTaskJSONObject.AsynctaskListner {
    ListView listOrders;
    ImageView selectAll;
    TextView consolidatebill;
    HashMap<String, ArrayList<JSONObject>> checkHashMap=new HashMap<>();
    ArrayList<JSONObject> dataString=new ArrayList<>();
    ArrayList<String> sortedString=new ArrayList<>();
    Double totalAmount;
    ArrayList<Double> referList=new ArrayList<>();
    ArrayList<Double> subreferList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seperate_list);
        context=this;
        callBackListner=this;
        back();
        drawerInit();

        setHeaderValue("Order Seperation");

        listOrders= (ListView) findViewById(R.id.list_data);
        selectAll= (ImageView) findViewById(R.id.selectAll);
        consolidatebill= (TextView) findViewById(R.id.consolidatebill);



        listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<JSONObject> orderId=checkHashMap.get(sortedString.get(position));
                ArrayList<String> order_ids=new ArrayList<String>();
                Utils.write("cart_id size      "+ orderId.size());

                for(int i=0; i<orderId.size(); i++){
                    try {
                        order_ids.add(orderId.get(i).getString("cart_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                String  order_id_concat=android.text.TextUtils.join("-", order_ids);
                CommonUtils.savePreferencesString(context,"cart_id",order_id_concat);
                Utils.write("cart_id       "+ order_id_concat);
                startActivity(new Intent(context,CheffUserOrderDetail.class));}
        });

        consolidatebill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        userListOrder();
        selectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteALLOrder();
                // order seperation deletion

            }
        });


   }


    protected void deleteALLOrder() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setMessage("Do you want to delete all orders?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int id) {
                        deleteALLDATA(CommonUtils.getPreferencesString(context,"user_list_id"));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        selectAll.setVisibility(View.VISIBLE);
    }

    @Override
    public void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status) {


        if(action.equalsIgnoreCase(AppConstant.SELECTED_ORDER)){
            Utils.write("data  inside         "+arr);

          try{
              JSONObject refer=new JSONObject(arr);
              Utils.showToastS(context,refer.getString("message"));
              userListOrder();
           }catch(Exception e){
            e.printStackTrace();
           }
        }else if(action.equalsIgnoreCase(AppConstant.DELETE_USER)){
            Utils.showToastS(context,"Deleted Successfully");

            listOrders.setAdapter(null);
            consolidatebill.setVisibility(View.GONE);
             finish();
        }
        else {
            try {
                Utils.write("data  inside else        "+arr);
                checkHashMap.clear();
                dataString.clear();
                sortedString.clear();
                referList.clear();

                String var = "";
                JSONObject objectData = new JSONObject(arr);
                if(objectData.getString("error").equalsIgnoreCase("false"))
                {

                JSONArray dataArray = objectData.getJSONArray("data");

                for (int i = 0; i < dataArray.length(); i++) {
                    var = dataArray.getJSONObject(i).getString("rank");

                    if (!sortedString.contains(var)) {
                        sortedString.add(var);
                    }
                    dataString.add(dataArray.getJSONObject(i));

                    totalAmount=Double.parseDouble(dataArray.getJSONObject(i).getString("price"));
                    referList.add(totalAmount);

                }

                for (int k = 0; k < sortedString.size(); k++) {
                    ArrayList<JSONObject> temp = new ArrayList<>();

                    for (int j = 0; j < dataString.size(); j++) {
                        if (sortedString.get(k).equalsIgnoreCase(dataString.get(j).getString("rank"))) {
                            temp.add(dataString.get(j));
                        }
                    }
                    checkHashMap.put(sortedString.get(k), temp);
                }
                    listOrders.setAdapter(new SeperationAdapter(context, checkHashMap, sortedString, callBackListner));


                }else{
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void userListOrder(){
        Utils.write("id    "+CommonUtils.getPreferencesString(context,"user_list_id"));
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id", CommonUtils.getPreferencesString(context,"user_list_id"));
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.SEPERATE_ORDER);
    }




   public void dialog() throws JSONException {
        final TextView billing_amount,text,ok;
        LinearLayout addView;
        final Dialog dialog = new Dialog(OrderSeperation.this);
      //  dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.y = 200;
        window.setAttributes(wlp);
        dialog.setContentView(R.layout.show_billing);

        addView = (LinearLayout) dialog.findViewById(R.id.addView);
        billing_amount = (TextView) dialog.findViewById(R.id.billing_amount);

        /*notnow.setText(""+checkHashMap.size());
        text.setText(""+sum(referList));*/
        final TextView[] myTextViews = new TextView[checkHashMap.size()];
        for(int k=0;k<checkHashMap.size();k++){
            final TextView rowTextView = new TextView(this);
            rowTextView.setTextSize(15.0f);
            rowTextView.setGravity(Gravity.CENTER_HORIZONTAL);
            // set some properties of rowTextView or something
            int j=k;
            int m=j+1;
            rowTextView.setTextColor(Color.parseColor("#000000"));


            subreferList=new ArrayList<>();
            for(int l=0;l<checkHashMap.get(sortedString.get(k)).size();l++){
                subreferList.add(Double.parseDouble(checkHashMap.get(sortedString.get(k)).get(l).getString("price")));
            }

                rowTextView.setText("BILL  "+m+  " :"+"    RP "+sum(subreferList));
            addView.addView(rowTextView);

            myTextViews[k] = rowTextView;
        }

        billing_amount.setText("RP "+sum(referList));
        dialog.show();
    }

    public void deleteALLDATA(String user_id){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("user_id",user_id);
        new AsyncTaskJSONObject(true, params, callBackListner, context, AppConstant.DELETE_USER);
    }

    public Double sum(ArrayList<Double> list) {
        Double sum = 0.00;
        for (Double i : list)
            sum = sum + i;
        return sum;
    }
}
