package com.villasfood;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import com.constants.Utils;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by firemoon on 25/5/17.
 */

public class AudioNotifiaction extends Activity implements TextToSpeech.OnInitListener {

    Activity context;
    private TextToSpeech myTTS;   // Define the TTS objecy
    private int MY_DATA_CHECK_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        Utils.write("onCreateworks           "+getIntent().getStringExtra("message"));
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
    }

    public void callToAudioNotification(String speakNotification){
        myTTS = new TextToSpeech(context, this);
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
        speakWords(speakNotification);
    }

    private void speakWords(String speech) {
        myTTS.speak(speech, TextToSpeech.LANG_COUNTRY_AVAILABLE, null);
    }



    @SuppressWarnings("deprecation")
    private void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        myTTS.setLanguage(Locale.US);
        myTTS.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void ttsGreater21(String text) {

        String utteranceId=this.hashCode() + "";
        myTTS.setLanguage(Locale.US);
        myTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        System.out.println("entry right");
    }

    public void onInit(int i)
    {

        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        myTTS.setLanguage(Locale.US);

        myTTS.speak(getIntent().getStringExtra("message"),
                TextToSpeech.QUEUE_FLUSH,  // Drop all pending entries in the playback queue.
                map);


        myTTS.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
            @Override
            public void onUtteranceCompleted(String utteranceId) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                       finish();
                    }
                });
            }
        });

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == MY_DATA_CHECK_CODE)
        {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS)
            {
                // success, create the TTS instance
                myTTS = new TextToSpeech(this, this);
            }
            else
            {
                // missing data, install it
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }
    @Override
    protected void onDestroy() {

        if(myTTS != null) {

            myTTS.stop();
            myTTS.shutdown();
        }
        super.onDestroy();


    }
}
