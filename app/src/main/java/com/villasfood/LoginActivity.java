package com.villasfood;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.villasfood.R;
import com.constants.AppConstant;
import com.constants.AsyncTaskJSONObject;
import com.constants.BaseActivity;
import com.constants.CommonUtils;
import com.constants.SessionManagement;
import com.constants.Utils;
import com.google.firebase.iid.FirebaseInstanceId;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by firemoon on 16/3/17.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener , AsyncTaskJSONObject.AsynctaskListner{
    public static boolean checkCheff;
    private Toolbar toolbar;
    private ImageView iv_fb, iv_gplus;
    private TextView tv_sign_in, tv_new_user_signup,tv_login, tv_terms, tv_policy, tvHeader,tv_agree;
    private EditText et_email, et_password;
    private Intent intent;
    private LinearLayout lltermslogin;
    private Animation animScale;
    private Handler handler;
    private Activity context;
    public static final int RC_SIGN_UP = 1;
    private String regid;
    private String deviceID = "";
    public static String gcmIDD = "";
    SessionManagement session;
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;

    @Override
    public void AsynctaskResult(String arr, String action,
                                HashMap<String, Object> webservices_params, boolean status) {
        try{

            System.out.println(arr);

            JSONObject jsonObject=new JSONObject(arr);

            if(jsonObject.getString("error").equalsIgnoreCase("false")){

                  checkCheff=true;


//                JSONObject data=jsonObject.getJSONObject("data");
//                CommonUtils.savePreferencesString(context,"user_id",data.getString("user_id"));
//                CommonUtils.savePreferencesString(context,"email",data.getString("email"));
//                CommonUtils.savePreferencesString(context,"firstname",data.getString("firstname")); f
                /* show email into monitor*/

                  System.out.println("print     "+jsonObject.getJSONArray("data").getJSONObject(0).getString("email"));

                /* show email into monitor*/

                session=new SessionManagement(LoginActivity.this);

                /*session saving data*/
                for (int j=0;j<jsonObject.getJSONArray("data").length();j++)
                    session.createLoginSession(jsonObject.getJSONArray("data").getJSONObject(j).getString("email"),jsonObject.getJSONArray("data").getJSONObject(j).getString("firstname"));
                /*session saving data*/

                CommonUtils.savePreferencesString(context,"from","cheff");
                CommonUtils.savePreferencesString(context,"email",jsonObject.getJSONArray("data").getJSONObject(0).getString("email"));
                CommonUtils.savePreferencesString(context,"user_id",jsonObject.getJSONArray("data").getJSONObject(0).getString("user_id"));
                CommonUtils.savePreferencesString(context,"firstname",jsonObject.getJSONArray("data").getJSONObject(0).getString("firstname"));
                startActivity(new Intent(LoginActivity.this,BackUpActivityCheff.class));
                finish();
            }else{
                CommonUtils.showToast(context,jsonObject.getString("message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null ) {
            if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.INTERNET,Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WAKE_LOCK}, 200);
            }else{
                setContentView(R.layout.cheff_login_activity);
                getid();
                setListner();
                callBackListner = this;
                context=this;
                handler = new Handler();
                animScale = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.anim_bounce);
                System.out.println("entry inner else");
            }

        }else{
            setContentView(R.layout.cheff_login_activity);
            getid();
            setListner();
            callBackListner = this;
            context=this;
            handler = new Handler();
            animScale = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.anim_bounce);
//            gotoInside();
            System.out.println("entry outer else ");
         }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    setContentView(R.layout.cheff_login_activity);
                    getid();
                    setListner();
                    callBackListner = this;
                    context=this;
                    handler = new Handler();
                    animScale = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.anim_bounce);
//                    gotoInside();
                }
                break;
        }
    }

    protected void makeVolley(){
        HashMap<String, Object> params = new LinkedHashMap<>();
        params.put("email", et_email.getText().toString().trim());
        params.put("password", et_password.getText().toString().trim());
        params.put("device_id",AppConstant.token);
        new AsyncTaskJSONObject(true, params, callBackListner, context, "http://www.avalon.villas/foodapp/v1/login");
        Utils.write("token               "+AppConstant.token);
    }

    private void getid() {

        tv_new_user_signup = (TextView) findViewById(R.id.registration);
        tv_login = (TextView) findViewById(R.id.sigin);
        et_email = (EditText) findViewById(R.id.username_cheff);
        et_password = (EditText) findViewById(R.id.password_cheff);
        et_email.setFocusable(false);
        et_email.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et_email.setFocusableInTouchMode(true);

                return false;
            }
        });
        tv_login.setTypeface(Utils.setFontText(LoginActivity.this));

    }

    private void setListner() {
        tv_login.setOnClickListener(this);
        tv_new_user_signup.setOnClickListener(this);
    }


    public boolean CheckValidations() {

        if (et_email.getText().toString().trim().length() == 0) {
            et_email.setError("Please enter Email id.");
            et_email.requestFocus();
            return false;
        }
        else if (!CommonUtils.isValidEmail(et_email.getText().toString().trim())) {
            et_email.setError("Please enter valid Email id.");
            et_email.requestFocus();
            return false;
        }
        else if (et_password.getText().toString().trim().length() == 0) {
            et_password.setError("Please enter password.");
            et_password.requestFocus();
            return false;

        } else if (et_password.getText().toString().trim().length() < 8) {
            et_password.setError("Minimum length of password should be of 8 characters.");
            et_password.requestFocus();
            return false;

        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sigin:
                AppConstant.token = FirebaseInstanceId.getInstance().getToken();
                tv_login.setAnimation(animScale);
                tv_login.startAnimation(animScale);

                CommonUtils.hideKeyPad(LoginActivity.this);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (CheckValidations()) {
                            CommonUtils.hideKeyPad(context);
                            if (CommonUtils.isNetworkConnected(LoginActivity.this)) {
                                makeVolley();
                            } else {
                                CommonUtils.showToastAlert(LoginActivity.this, "connection error");
                            }
                        }
                    }
                }, AppConstant.BOUNCING_EFFECT_TIME);
                break;

            case R.id.registration:
                tv_new_user_signup.setAnimation(animScale);
                tv_new_user_signup.startAnimation(animScale);
                CommonUtils.hideKeyPad(LoginActivity.this);

                handler.postDelayed(new Runnable() {
                    public void run() {
                        intent = new Intent(LoginActivity.this, SignUpActivityCheff.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }, AppConstant.BOUNCING_EFFECT_TIME);
                break;

        }
    }}

//