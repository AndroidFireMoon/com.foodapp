package com.constants;

/**
 * Created by ashutoshkumar on 12/10/15.
 */
public class AppConstant {
    public static String profileFlag="null";
    public static final String BASEURL = "http://www.avalon.villas/foodapp/v1/";

    public static final String ADD_CATEGORY = BASEURL+"addCatogry";
    public static final String LSIT_CATEGORY = BASEURL+"listCatogry";
    public static final String EDIT_CATEGORY = BASEURL+"updateCatogry";

    public static final String ADD_ITEM = BASEURL+"addItem";
    public static final String LIST_ITEM = BASEURL+"listItem";
    public static final String EDIT_ITEM = BASEURL+"updateItem";
    public static final String UPDATE_PROFILE = BASEURL+"updateProfile";

    public static final String CAT_LIST = BASEURL+"getCategoryItem";
    public static final String ADD_CART = BASEURL+"addCart";
    public static final String CART_LIST = BASEURL+"cartList";
    public static final String CLEAR_LIST = BASEURL+"clearCartById";
    public static final String CONFIRM_ORDER = BASEURL+"confirmOrder";

    public static final String ORDER_LIST = BASEURL+"orderListByUser";
    public static final String USER_LIST = BASEURL+"userList";

    public static final String ORDER_STATUS = BASEURL+"changeOrderStatus";
    public static final String Delete_Order = BASEURL+"deleteOrder";


    public static final String PAID_BILLS = BASEURL+"savePaidBills";
    public static final String DELETE_USER = BASEURL+"deleteAllOrders";
    public static final String SEPERATE_ORDER = BASEURL+"ordersOfUser";

    public static final String SELECTED_ORDER = BASEURL+"deleteSelectedOrder";
    public static final String MAPPING_ORDER = BASEURL+"ItemsOfOrder";

    public static final String UPLOAD_ITEM = BASEURL+"updateSpecialItem";

    public static final String GET_ITEM = BASEURL+"specialItems";

    public static final String DELETE_ITEM = BASEURL+"deleteSpecialItem";

    public static final String USER_PROFILE = BASEURL+"updateUserProfile";

    public static final String USER_IMAAGE = BASEURL+"userImage";


    public static final String DELETE_CAT = BASEURL+"deleteCategory";

    public static final String DELETE_ITM = BASEURL+"deleteItem";






























    //end

    public static final String FLIGHT_DATE = "FLIGHT_DATE";
    /*    public static String SECONDARY_EMAIL_1 = "mailID";
            public static String SECONDARY_EMAIL_2 = "mailID";
            public static String SECONDARY_EMAIL_3 = "mailID";
            public static String SECONDARY_EMAIL_4 = "mailID";
            public static String SECONDARY_EMAIL_5 = "mailID";*/
    public static String font_ftrabk = "FTRABK.TTF";
    public static String font_ftrabki = "FTRABKI.TTF";
    public static String font_ftrahv = "GIL.TTF";
    public static String font_futuralHV = "FuturaHeavy.ttf";

    public static final String GCM_ID = "GCM_ID";
    public static final String DEVICE_ID = "DEVICE_ID";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";


    public static final String ORIGIN_CITY = "ORIGIN_CITY";
    public static final String DESTINATIONCITY = "DESTINATIONCITY";


    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_PASS = "USER_PASS";
    public static final String IS_USER_LOGIN = "is_user_login";
    public static final String SEARCH_FLIGHT_NUMBER_KEY = "search_flight_number_key";
    public static final String SEARCH_BY_ROUTE = "search_by_route";
    public static final String SEARCH_DATA = "search_data";
    public static final String SEARCH_FLIGHT_ORIGIN_AIRPORT_RESULT_KEY = "search_flight_origin_airport_result_key";
    public static final String SEARCH_FLIGHT_DESTINATION_AIRPORT_RESULT_KEY = "search_flight_destination_airport_result_key";
    public static final String SEARCH_FLIGHT_KEY = "search_flight_key";
    public static final String SEARCH_FLIGHT_CODE = "search_flight_code";

    public static final String SELECTED_AIRPORT_CITY = "selected_airport_city";
    public static final String SELECTED_AIRPORT_NAME = "selected_airport_name";
    public static final String SELECTED_AIRPORT_CODE = "selected_airport_code";
    public static final String SEARCH_FLIGHT_DESTINATION_CITY_CODE = "search_flight_destination_city_code";
    public static final String SEARCH_FLIGHT_ORIGIN_CITY_CODE = "search_flight_origin_city_code";

    public static final String SEARCH_FLIGHT_ORIGIN_CITY_NAME_CODE = "search_flight_origin_city_name_code";
    public static final String SEARCH_FLIGHT_DESTINATION_CITY_NAME_CODE = "search_flight_destination_city_name_code";
    public static final String AIRLINE_IMAGE_HORIZONTAL = "airline_image";
    public static final String AIRPORT_NAME = "airport_name";
    public static final String AIRPORT_DEP_TYPE = "AIRPORT_DEP_TYPE";
    public static final String FILTER_FROM_OFFERS = "filter";
    public static final String SHARE_URL = "share_url";
    public static final String CHECK_IN = "CHECK_IN";
    public static final String BOARD_IN = "BOARD_IN";
    public static final String TAKE_OFF = "TAKE_OFF";
    public static final String LANDING = "LANDING";

    public static final String DEPARTURE_TERMINAL = "departure_terminal";
    public static final String DEPARTURE_GATE = "departure_gate";
    public static final String ONLINE_CHECK_URL = "ONLINE_CHECK_URL";
    public static final String HAS_BAGGAGE = "HAS_BAGGAGE";
    public static final String ARRIVAL_TERMINAL = "ARRIVAL_TERMINAL";
    public static final String ARRIVAL_GATE = "ARRIVAL_GATE";
    public static final String LUGGAGE_BELT = "LUGGAGE_BELT";

    /*
        constants for data
    */

    public static String SENDER_ID = "945056857275";
    public static String SERVER_NOT_RESPOND = "Server not responding.";
    public static final String DEVICE_TYPE = "android";
//    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";

    /*
    constants for action in web service
     */

    public static final String actionUpdatePassword = "updatePassword";
    public static final String actionSearchFlightByNumber = "searchFlightByNo";
    public static final String actionUpdateProfile = "userUpdate";
    public static final String actionUserProfile = "userProfile";
    public static final String actionAddUserProfileImage = "adduserProfileImage";
    public static final String actionForgotPassword = "forgotPassword";
    public static final String actionLogout = "logout";
    public static final String actionSearchFLightByRoute = "searchFlightByRoute";
    public static final String actionGetAirport = "getAirport";
    public static final String actionGetPopularRecentAirport = "getPopularRecent";
    public static final String actionGetAirlines = "getAirlines";
    public static final String actionGetPopularRecentAirlines = "getRecentAirlines";
    public static final String actionUserMailDelete = "userMail_delete";
    public static final String actionGetBoardingPass = "getBordingPass";


    public static final String NAME = "name_user_name";
    public static final String AIRPORT_ACTIVITY_KEY = "airport_activity_key";
    public static final String AIRPORT_ACTIVITY_DESTINATION = "airport_activity_destination";
    public static final String AIRPORT_ACTIVITY_ORIGIN = "airport_activity_origin";
    public static final String AIRLINE_ACTIVITY_KEY = "airline_activity_key";


    public static final String FLIGHT_ID = "flight_id";
    public static final String AIRPORT_ARRIVAL_ID = "AIRPORT_ARRIVAL_ID";
    public static final String AIRPORT_ARRIVAL_NAME = "AIRPORT_ARRIVAL_NAME";
    public static final String AIRPORT_DEPARTURE_ID = "AIRPORT_DEPARTURE_ID";
    public static final String AIRPORT_DEPARTURE_NAME = "AIRPORT_DEPARTURE_NAME";
    public static final String AIRPORT_USER_ID = "AIRPORT_USER_ID";
    public static final String AIRLINE_ID = "AIRLINE_ID";
    public static final String AIRLINE_NAME = "AIRLINE_NAME";
    public static final String AIRPORT_ID = "AIRPORT_ID";
    public static final String IS_FROM_FAVORITE = "IS_FROM_FAVORITE";
    public static final String IS_FROM_MYFLIGHT = "IS_FROM_MYFLIGHT";
    public static final String OFFER_ID = "OFFER_ID";
    public static final String Is_Favourite_Offers = "Is_Favourite_Offers";
    public static final int BOUNCING_EFFECT_TIME = 300;
    public static String filterType="";
    public static String FilterCallFragment="";
//    public static List<TerminalListModel> selectedTerminalList=new ArrayList<TerminalListModel>();
    public static final String PAGE_SIZE = "10";

    public static final String AIRPORT_TYPE = "AIRPORT_TYPE";

    public static final String EATERIES = "EATERIES";
    public static final String SHOPPING = "SHOPPING";
    public static final String SERVICES = "SERVICES";
    public static final String FILTER = "FILTER";
    public static final String AMINITY_ID = "AMINITY_ID";
    public static final String AMINITY_NAME = "AMINITY_NAME";
    public static final String IS_FIRST_INTRO = "IS_FIRST_INTRO";
    public static final String INTRO_PREF = "INTRO_PREF";
    public static final String MESSAGE = "MESSAGE";
    public static final String TYPE = "TYPE";
    public static final String FROM_GCM = "FROM_GCM";
    public static String token="";


//    public static final String AIRPORT_RESPONSE = "AIRPORT_RESPONSE";




}
