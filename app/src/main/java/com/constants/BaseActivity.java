package com.constants;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.villasfood.LoginActivity;
import com.villasfood.LoginUser;
import com.villasfood.MainActivity;
import com.villasfood.MyOrders;
import com.villasfood.ProfileActivity;
import com.villasfood.ProfileForUser;
import com.villasfood.R;
import com.villasfood.UserOrders;
import com.fragment.NavigationDrawerFragment;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("NewApi")
public class BaseActivity extends AppCompatActivity implements OnClickListener,NavigationDrawerFragment.FragmentDrawerListener{
    DrawerLayout drawerLayout;
    NavigationDrawerFragment drawerFragment;
    private Toolbar mToolbar;
    public AsyncTaskJSONObject.AsynctaskListner callBackListner;
    public Activity context;


    public void setNextActiveFragment(Fragment fragment, boolean addToBackStack) {

        if (addToBackStack) {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.cotainer_frame, fragment)
                    .addToBackStack(null)
                    .commit();
        } else {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.cotainer_frame, fragment)
                    .commit();
        }
    }

        public void back(){
           ImageView backFinish= (ImageView) findViewById(R.id.backFinish);
            backFinish.setVisibility(View.VISIBLE);
            backFinish.setOnClickListener(new OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  finish();
                                              }
                                          }
            );
        }

    public void setHeaderValue(String title){
        TextView setHeader= (TextView) findViewById(R.id.setHeader);
        setHeader.setText(title);
    }


    public void drawerInit(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//       mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        drawerLayout=(DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout_drawer);
        drawerFragment.setUp(R.id.fragment_layout_drawer, drawerLayout, mToolbar);
        drawerFragment.setDrawerListener(this);
    }

    public boolean checkNetworkConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
    }

    public void sendEmail(String arr) {
        String[] TO = {"ashishfiremoon@gmail.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, arr);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            // finish();

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected static void checkAndCloseActivity(Activity context) {
        try {

            if (context != null && !context.isFinishing()) {
                context.finish();
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    protected String returnblankifnull(String text) {

        if (text != null && text.length() > 0) {
            return text;
        }
        return "";

    }

    public static void setupUI(View view, final Activity activity) {

        // Set up touch listener for non-text box views to hide keyboard.
        System.out.println("touch");
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    Utils.hideSoftKeyboard(activity);
                    return false;
                }

            });
        }

    }

    @SuppressLint("SimpleDateFormat")
    protected String changeDateFormat(String date) {
        String str = "";
        try {
            SimpleDateFormat inputFormatter11 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date inputdate = inputFormatter11.parse(date);
            SimpleDateFormat outputFormat = new SimpleDateFormat("MM - dd - yyyy  hh:mm a");
            str = outputFormat.format(inputdate);

            Utils.write("ashih===== " + str);
        } catch (Exception e) {
            str = date;
            // TODO: handle exception
        }
        return str;
    }


    @SuppressLint("SimpleDateFormat")
    protected String simpleDateFormat(String date) {
        String str = "";
        try {
            SimpleDateFormat inputFormatter11 = new SimpleDateFormat("dd-MM-yyyy");
            Date inputdate = inputFormatter11.parse(date);
            SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            str = outputFormat.format(inputdate);

            Utils.write("ashih======= " + str);
        } catch (Exception e) {
            str = date;
            e.printStackTrace();
            // TODO: handle exception
        }
        return str;
    }



    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        switch (position) {
            case 0:
                Intent intent= new Intent(context, MainActivity.class);
                startActivity(intent);
                break;
            case 1:

                if(AppConstant.profileFlag.equalsIgnoreCase("user")){
                    if(CommonUtils.getPreferencesString(context,"check").equalsIgnoreCase("success")){
                        Utils.showToastS(context,"Already logged in");
                    }else{

                        Intent intent3= new Intent(context, LoginUser.class);
                        startActivity(intent3);
                    }
                }else {
                    if(!CommonUtils.getPreferencesString(context,"email").equalsIgnoreCase("")){
                        Utils.showToastS(context,"Already logged in");
                    }else{
                    Intent intent3= new Intent(context, LoginActivity.class);
                    startActivity(intent3);
                 }
                }


                break;
            case 2:
                if(AppConstant.profileFlag.equalsIgnoreCase("user")){
                    Intent intent1 = new Intent(context, ProfileForUser.class);
                    startActivity(intent1);
                }else {
                    Intent intent1 = new Intent(context, ProfileActivity.class);
                    startActivity(intent1);
                }
                break;
            case 3:
                if(AppConstant.profileFlag.equalsIgnoreCase("user")){
                    Intent intent4= new Intent(context, MyOrders.class);
                    startActivity(intent4);
                }
                else{
                    Intent intent4= new Intent(context, UserOrders.class);
                    startActivity(intent4);
                }

                break;
            case 4:
                Intent intent8=new Intent(Intent.ACTION_VIEW);
                intent8.setData(Uri.parse("https://www.tripadvisor.com/VacationRentalReview-g3185061-d7625319-Avalon_Ubud_Carved_out_of_Mountain_in_Ubud_with_Swimming_pool-Mas_Ubud_Bali.html"));
                startActivity(intent8);
                break;
            case 5:
                exitFromApp();
                break;
        }
    }
    protected void exitFromApp() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setMessage("Do you want to exit from app?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int id) {
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                        context.sendBroadcast(broadcastIntent);
                        context.finishAffinity();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }


}
