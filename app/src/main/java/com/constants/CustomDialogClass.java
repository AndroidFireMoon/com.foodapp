package com.constants;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.villasfood.R;

/**
 * Created by firemoon on 29/7/17.
 */

public class CustomDialogClass extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button yes, no;
    TextView t1,t2,t3;
    String s1,s2,s3;

    public CustomDialogClass(Activity a, String price, String order_date, String order_id) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        s1=order_id;
        s2=price;
        s3=order_date;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.show_billing);
/*
        t1= (TextView) findViewById(R.id.billing_number);
        t2= (TextView) findViewById(R.id.billing_amount);
        t3= (TextView) findViewById(R.id.billing_date);

        t1.setText(s1);
        t2.setText(s2);
        t3.setText(s3);*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            default:
                break;
        }
        dismiss();
    }
}