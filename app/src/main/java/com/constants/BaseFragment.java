package com.constants;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.villasfood.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by firemoon on 20/3/17.
 */

public class BaseFragment extends Fragment implements View.OnClickListener {


    public void setNextActiveFragment(Fragment fragment, boolean addToBackStack) {


        if (addToBackStack) {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.cotainer_frame, fragment)
                    .addToBackStack(null)
                    .commit();
        } else {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.cotainer_frame, fragment)
                    .commit();
        }



    }


    @Override
    public void onClick(View v) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getActivity();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public Activity context;

    public boolean checkNetworkConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    public void sendEmail(String arr) {
        String[] TO = {"ashishfiremoons@gmail.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, arr);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            // finish();

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There is no email client installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected static void checkAndCloseActivity(Activity context) {
        try {

            if (context != null && !context.isFinishing()) {
                context.finish();
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    protected String returnblankifnull(String text) {

        if (text != null && text.length() > 0) {
            return text;
        }
        return "";

    }

    public static void setupUI(View view, final Activity activity) {

        // Set up touch listener for non-text box views to hide keyboard.
        System.out.println("touch");
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    Utils.hideSoftKeyboard(activity);
                    return false;
                }

            });
        }

    }

    @SuppressLint("SimpleDateFormat")
    protected String changeDateFormat(String date) {
        String str = "";
        try {
            SimpleDateFormat inputFormatter11 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date inputdate = inputFormatter11.parse(date);
            SimpleDateFormat outputFormat = new SimpleDateFormat("MM - dd - yyyy  hh:mm a");
            str = outputFormat.format(inputdate);

            Utils.write("ashih===== " + str);
        } catch (Exception e) {
            str = date;
            // TODO: handle exception
        }
        return str;
    }


    @SuppressLint("SimpleDateFormat")
    protected String simpleDateFormat(String date) {
        String str = "";
        try {
            SimpleDateFormat inputFormatter11 = new SimpleDateFormat("dd-MM-yyyy");
            Date inputdate = inputFormatter11.parse(date);
            SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            str = outputFormat.format(inputdate);

            Utils.write("ashih======= " + str);
        } catch (Exception e) {
            str = date;
            e.printStackTrace();
            // TODO: handle exception
        }
        return str;
    }
}
