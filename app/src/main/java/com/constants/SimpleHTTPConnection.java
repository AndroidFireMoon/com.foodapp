package com.constants;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class SimpleHTTPConnection {

    private static final String TAG = "SimpleHTTPConnection";
    private static final String ERROR = "Simple HTTP Connection Error";

    public static String sendByPOST(Context context, String url,
                                    ArrayList<NameValuePair> data) {
        InputStream is;
        StringBuilder sb;
        String result = ERROR;
       {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(data));
//                httppost.addHeader("X-API-KEY", Utils.getPrefrence(context, Constants.session_id));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is, "iso-8859-1"), 20);
                sb = new StringBuilder();
                sb.append(reader.readLine());
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                result = sb.toString();
            } catch (IllegalStateException e) {
                // Log.i(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            } catch (UnknownHostException e) {
                // Log.i(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            } catch (UnsupportedEncodingException e) {
                // Log.i(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            } catch (ClientProtocolException e) {
                // Log.i(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                // Log.i(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return result;
    }

    public static String sendByGET(String url) {
        InputStream is;
        StringBuilder sb;
        String result = ERROR;

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 20);
            sb = new StringBuilder();
            sb.append(reader.readLine());
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);

            }
            is.close();
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    /*
     * public static String sendImageData(String url,byte[] data,String
     * name,String email,String phone){ try { HttpClient httpClient = new
     * DefaultHttpClient(); HttpContext localContext = new BasicHttpContext();
     * HttpPost httpPost = new HttpPost(url);
     *
     * MultipartEntity entity = new
     * MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
     * entity.addPart("username", new StringBody(name)); entity.addPart("email",
     * new StringBody(email)); entity.addPart("avatar", new
     * ByteArrayBody(data,"image1.jpeg")); entity.addPart("phone", new
     * StringBody(phone)); httpPost.setEntity(entity); HttpResponse response =
     * httpClient.execute(httpPost, localContext); BufferedReader reader = new
     * BufferedReader( new InputStreamReader( response.getEntity().getContent(),
     * "UTF-8"));
     * String sResponse = reader.readLine(); return sResponse; } catch
     * (Exception e) {
     *
     * Log.e(e.getClass().getName(), e.getMessage(), e); return null; } }
     */
    // With image upload using post
    public static String sendByPOST(HttpPost httppost)// (String url,
    // ArrayList<NameValuePair>
    // data)
    {
        InputStream is;
        StringBuilder sb;
        String result = ERROR;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            // HttpPost httppost = new HttpPost (url);
            // httppost.setEntity (new UrlEncodedFormEntity(data));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 20);
            sb = new StringBuilder();
            sb.append(reader.readLine());
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);

            }
            is.close();
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    // With image upload using put
    public static String sendByPUT(HttpPut httpput)// (String url,
    // ArrayList<NameValuePair>
    // data)
    {
        InputStream is;
        StringBuilder sb;
        String result = ERROR;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            // HttpPost httppost = new HttpPost (url);
            // httppost.setEntity (new UrlEncodedFormEntity(data));
            HttpResponse response = httpclient.execute(httpput);
            HttpEntity entity = response.getEntity();

            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 20);
            sb = new StringBuilder();
            sb.append(reader.readLine());
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);

            }
            is.close();
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    public static String sendByPUT(String url, ArrayList<NameValuePair> data) {
        InputStream is;
        StringBuilder sb;
        String result = ERROR;

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPut httpput = new HttpPut(url);
            httpput.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = httpclient.execute(httpput);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 20);
            sb = new StringBuilder();
            sb.append(reader.readLine());
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);

            }
            is.close();
            result = sb.toString();

        } catch (UnsupportedEncodingException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    public static ArrayList<NameValuePair> generateParams(String[] keys,
                                                          String[] values) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        for (int i = 0; i < keys.length; i++) {
            params.add(new BasicNameValuePair(keys[i], values[i]));
        }

        return params;
    }

    public static String buildGetUrl(String url, String[] keys, String[] values) {
        if (!url.endsWith("?"))
            url += "?";
        url += URLEncodedUtils.format(generateParams(keys, values), "utf-8");

        return url;
    }

}
