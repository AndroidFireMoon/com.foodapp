package com.constants;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	public static String getPreferencesString(Context context, String key) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPreferences.getString(key, "");

	}

	/*public static Typeface setFontText(Context context) {
		Typeface tf_light = Typeface.createFromAsset(context.getAssets(), AppConstant.font_ftrahv);
		return tf_light;
	}*/

	public static Typeface setFontText(Context context) {
		Typeface tf_regular = Typeface.createFromAsset(context.getAssets(), AppConstant.font_ftrahv);
		return tf_regular;
	}

	public static void savePreferencesString(Context context, String key, String value) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void removePreferencesString(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.remove(key);
		editor.commit();
	}

	public static ArrayList<Activity> activityArrayList = new ArrayList<Activity>();

	public static void add(Activity activity) {
		activityArrayList.add(activity);
	}

	public static void remove(Activity activity) {
		activityArrayList.remove(activity);
	}

	public static Bundle convertmaptobundle(
			Map<String, Object> webservices_params) {
		// TODO Auto-generated method stub
		Bundle b = new Bundle();

		if (webservices_params != null) {
			for (String key : webservices_params.keySet()) {
				Object value = webservices_params.get(key);
				if (value instanceof Integer) {
					b.putString(key, String.valueOf((Integer) value));
				} else if (value instanceof Long) {
					b.putString(key, String.valueOf((Long) value));
				} else if (value instanceof String) {
					b.putString(key, (String) value);
				}
			}
		}

		return b;
	}

	private Map<String, Object> convertbundletomap(Bundle webservices_params) {
		// TODO Auto-generated method stub
		Map<String, Object> b = new HashMap<String, Object>();

		if (webservices_params != null) {
			for (String key : webservices_params.keySet()) {
				Object value = webservices_params.get(key);
				b.put(key, value);
			}
		}

		return b;
	}

	public static boolean checkNetworkConnection(Context c) {
		ConnectivityManager connectivityManager = (ConnectivityManager) c
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public static String setLeadingZero(int no) {
		String str = "" + no;
		if (no < 10)
			str = "0" + no;
		return str.trim();
	}

	public static JSONArray removeJsonObjectAtJsonArrayIndex(JSONArray source, int index) throws JSONException {
		if (index < 0 || index > source.length() - 1) {
			throw new IndexOutOfBoundsException();
		}

		final JSONArray copy = new JSONArray();
		for (int i = 0, count = source.length(); i < count; i++) {
			if (i != index) copy.put(source.get(i));
		}
		return copy;
	}

	public static void killAllActivity() {
		for (int i = 0; i < activityArrayList.size(); i++) {
			if (!activityArrayList.get(i).isFinishing()) {
				activityArrayList.get(i).finish();
			}
		}
		activityArrayList.clear();
	}

	public static String encodeURL(String urlStr) {

		URL url = null;
		try {
			// System.out.println("image url=="+urlStr);
			if (urlStr != null) {
				if (urlStr.length() > 4) {
					if (urlStr.startsWith("http") || urlStr.contains("http://")) {
					} else
						urlStr = "http://" + urlStr;
					url = new URL(urlStr);
					URI uri = new URI(url.getProtocol(), url.getUserInfo(),
							url.getHost(), url.getPort(), url.getPath(),
							url.getQuery(), url.getRef());
					url = uri.toURL();
					return url.toString().replaceAll("&amp;", "&");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return urlStr;
	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;
		String expression = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static interface Method {
		void execute();
	}

	public static Dialog createSimpleDialog(Context context, String title,
			String msg, String btnLabel1, String btnLabel2,
			final Method method1, final Method method2) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setPositiveButton(btnLabel1,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (method1 != null)
							method1.execute();
					}
				});

		builder.setNegativeButton(btnLabel2,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (method2 != null)
							method2.execute();

					}
				});

		return builder.create();
	}

	public static Dialog createSimpleDialog(Context context, String title,
			String msg, String btnLabel, final Method method,
			AlertDialog.Builder builderdata) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builderdata = builder;
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton(btnLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						method.execute();
					}
				});

		return builder.create();
	}

	public static Dialog createInfoDialog(Context context, String title,
			String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		return builder.create();
	}

	public static void dumpIntent(Intent i) {

		Bundle bundle = i.getExtras();
		if (bundle != null) {
			Set<String> keys = bundle.keySet();
			Iterator<String> it = keys.iterator();
			while (it.hasNext()) {
				String key = it.next();
				Log.e("Intent Data", "[" + key + "=" + bundle.get(key) + "]");
			}

		}
	}

	public static void write(String message) {
		try {
			System.out.println(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void hideSoftKeyboard(Activity activity) {
		try {
			InputMethodManager inputManager = (InputMethodManager) activity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (activity.getCurrentFocus().getWindowToken() != null) {
				inputManager.hideSoftInputFromWindow(activity.getCurrentFocus()
						.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*public static void sendemail(Activity act, String text) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "amangupta.trivialworks@gmail.com" });
		i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
		i.putExtra(Intent.EXTRA_TEXT, text + "");
		try {
			act.startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		}
	}*/

	public static boolean isJsonObject(String data) {
		try {
			new JSONObject(data);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static String check(JSONObject object, String key) {
		try {
			if (object.has(key)) {
				String value = object.getString(key);
				if (TextUtils.isEmpty(value)) {
					return "";
				} else {
					return value;
				}
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static void showToastS(Context context, String text) {
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}



	public static String loadJSONFromAsset(Context context,String fileName) {
		String json = null;
		try {
			InputStream is = context.getAssets().open(fileName);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			json = new String(buffer, "UTF-8");
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;
	}

	public static boolean isConnectingToInternet(Context c) {
		try {
			ConnectivityManager connectivity = (ConnectivityManager) c
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null)
					for (int i = 0; i < info.length; i++)
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							return true;
						}
			}

			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// public static String getPin(Context context) {
	// // TODO Auto-generated method stub
	// // return "zbj17b67bv9espw4w40";
	// return getUserPrefrence(context)
	// .getString(AppConstants.AccessToken, "");
	// }
	//
	// public static void savePin(Context context, String value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit()
	// .putString(AppConstants.AccessToken, value).commit();
	// }
	//
	// public static String getUserName(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getString("name", "");
	// }
	//
	// public static void setRefreshList(Context context, boolean value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit().putBoolean("list", value).commit();
	// }
	//
	// public static boolean isRefreshList(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getBoolean("list", false);
	// }
	//
	// public static void saveUserName(Context context, String value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit().putString("name", value).commit();
	// }
	//
	// public static String getUserTag(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getString("tag", "");
	// }
	//
	// public static void saveUserTag(Context context, String value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit().putString("tag", value).commit();
	// }
	//
	// public static String getUserEmail(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getString("email", "");
	// }
	//
	// public static void saveUserEmail(Context context, String value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit().putString("email", value).commit();
	// }
	//
	// public static String getUserId(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getString(AppConstants.ID, "");
	// }
	//
	// public static void saveCustomerId(Context context, String value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit()
	// .putString(AppConstants.CustomerId, value).commit();
	// }
	//
	// public static String getCustomerId(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getString(AppConstants.CustomerId, "");
	// }
	//
	// public static void setFaceInfoSaved(Context context, Boolean value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit()
	// .putBoolean(AppConstants.faceInfo, value).commit();
	// }
	//
	// public static boolean getFaceInfoSaved(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context)
	// .getBoolean(AppConstants.faceInfo, true);
	// }
	//
	// public static boolean getObjectInfoSaved(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getBoolean(AppConstants.objectInfo,
	// false);
	// }
	//
	// public static void setObjectInfoSaved(Context context, Boolean value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit()
	// .putBoolean(AppConstants.objectInfo, value).commit();
	// }
	//
	// public static boolean isLoggedIn(Context context) {
	// // TODO Auto-generated method stub
	// return getUserPrefrence(context).getBoolean("loggedIn", false);
	// }
	//
	// public static void setLoggedIn(Context context, Boolean value) {
	// // TODO Auto-generated method stub
	// getUserPrefrence(context).edit().putBoolean("loggedIn", value).commit();
	// }

	public static void showAllData(Cursor cursor) {
		if (cursor != null) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				String row = "";
				for (int j = 0; j < cursor.getColumnCount(); j++) {
					String name = cursor.getColumnName(j);
					String value = cursor.getString(j);
					row += name + ">" + value + ",";
				}
				Utils.write(row);
				cursor.moveToNext();
			}
		} else {
			System.out.println("Null cursor found");
		}
	}

	public static ArrayList<HashMap<String, String>> getArrayList(Cursor cursor) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		if (cursor != null) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				HashMap<String, String> hashMap = new HashMap<String, String>();
				for (int j = 0; j < cursor.getColumnCount(); j++) {
					String name = cursor.getColumnName(j);
					String value = cursor.getString(j);
					hashMap.put(name, value);
				}
				arrayList.add(hashMap);
				cursor.moveToNext();
			}
			Utils.write(arrayList.toString());
		} else {
			System.out.println("Null cursor found");
		}
		return arrayList;
	}

	public static void closeCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		if (cursor != null) {
			cursor.close();
		}
	}

	public static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	public static String addZero(int value) {
		if (value > 9) {
			return value + "";
		} else {
			return "0" + value;
		}
	}

	public static String get12HourFormat(String string) {
		// TODO Auto-generated method stub
		try {
			String _24HourTime = string;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			Date _24HourDt = _24HourSDF.parse(_24HourTime);

			return _12HourSDF.format(_24HourDt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return string;
	}

	public static String convert12to24(String string) {
		// TODO Auto-generated method stub
		try {
			String _12HourTime = string;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			Date _12HourDt = _12HourSDF.parse(_12HourTime);

			return _24HourSDF.format(_12HourDt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return string;
	}

	// =====================for Loan calculator ====================

	public static double formattedStringToDouble(String s) {
		return Double.parseDouble(s.replaceAll("[a-zA-Z$,.\\s]", ""));
	}


/*	public static String getTimeStamp(Context context, String key) {
		SharedPreferences preferences = context.getSharedPreferences(
				Constant.timeStampPref, Context.MODE_PRIVATE);
		return preferences.getString(key, "");
	}

	public static void setTimeStamp(Context context, String key, String value) {
		SharedPreferences.Editor editor = context.getSharedPreferences(
				Constant.timeStampPref, Context.MODE_PRIVATE).edit();
		editor.putString(key, value);
		editor.commit();
	}*/

	public static HashMap<String, String> getHashMapFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		HashMap<String, String> hashMap = new HashMap<String, String>();
		if (cursor != null) {

			for (int j = 0; j < cursor.getColumnCount(); j++) {
				String name = cursor.getColumnName(j);
				String value = cursor.getString(j);
				hashMap.put(name, value);
			}
		}
		return hashMap;
	}

	public static void closeKeyboard(Context c, IBinder windowToken) {
		try {
			InputMethodManager mgr = (InputMethodManager) c
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			mgr.hideSoftInputFromWindow(windowToken, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String capitaliseData(String d) {
		final StringBuilder result = new StringBuilder(d.length());
		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(d);
		boolean b = m.find();

		if (b)
			System.out.println("There is a special character in my string");
		else {
			try {
				String[] words = d.split("\\s");
				for (int i = 0, l = words.length; i < l; ++i) {
					if (i > 0)
						result.append(" ");
					result.append(
							Character.toUpperCase(words[i].trim().charAt(0)))
							.append(words[i].substring(1));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result.toString();
	}

	public static String capitalLetter(String nam) {

		String[] splitName = nam.trim().split(" ");
		String naam = "";
		System.out.println("split name : " + splitName.toString());
		for (int i = 0; i < splitName.length; i++) {
			System.out.println("split name xyz : " + splitName[i]);
			if (!TextUtils.isEmpty(splitName[i])) {
				if (splitName[i].charAt(0) >= 'a'
						&& splitName[i].charAt(0) <= 'z') {
					char fl = (char) ((int) (splitName[i].charAt(0)) - 32);
					naam = naam + nam.split(" ")[i].replace(nam.charAt(0), fl);
				} else {
					naam = naam + nam.split(" ")[i];
				}
				if (i != splitName.length - 1) {
					naam += " ";
				}
			}
		}
		return naam.toString().trim();

	}

}
