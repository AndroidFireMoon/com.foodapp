package com.constants;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;

//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;

public class AsyncTaskJSONObject {
    public ProgressDialog progressDialog;
    String url = "";
    Activity context;
    AsynctaskListner callBackListner;
    String action;
    HashMap<String, Object> bundleData;
    boolean dialogShow;

    public AsyncTaskJSONObject(boolean dialogShow, final HashMap<String, Object> bundleData1,
                               AsynctaskListner callBackListner1, Activity context, String url1) {
        this.callBackListner = callBackListner1;
        this.context = context;
        this.url = url1;
        this.bundleData = bundleData1;
        this.action = url1;
        this.dialogShow = dialogShow;
        if (dialogShow) {
            if (progressDialog != null) {
                dismissDialog();
            }
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Please Wait");
                progressDialog.show();
            }
        }
//        if (Utils.checkNetworkConnection(context)) {
            setDatainBundle();
            new AsyncTaskApp1().execute();
//            postNewCommentretro();
//        } else {
//            dismissDialog();
//            Utils.showToastS("Please check your network connection!", context);
//        }
    }

//    public AsyncTaskJSONObject(String action, boolean dialogShow, final HashMap<String, Object> bundleData1,
//                               AsynctaskListner callBackListner1, Activity context, String url1) {
//        this.callBackListner = callBackListner1;
//        this.context = context;
//        this.url = url1;
//        this.bundleData = bundleData1;
//        this.action = action;
//        this.dialogShow = dialogShow;
//        if (dialogShow) {
//            if (progressDialog != null) {
//                dismissDialog();
//            }
//            progressDialog = new ProgressDialog(context);
//            progressDialog.setCancelable(false);
//            if (!progressDialog.isShowing()) {
//                progressDialog.setMessage("Please Wait");
//                progressDialog.show();
//            }
//        }
//        if (Utils.checkNetworkConnection(context)) {
//            setDatainBundle();
//            new AsyncTaskApp1().execute();
////            postNewCommentretro();
//        } else {
//            dismissDialog();
//            Utils.showToastS("Please check your network connection!", context);
//        }
//    }

    public static String wrap_string(Object o) {
        if (o == null) {
            return null;
        }
        try {

            if (o instanceof Boolean ||
                    o instanceof Byte ||
                    o instanceof Character ||
                    o instanceof Double ||
                    o instanceof Float ||
                    o instanceof Integer ||
                    o instanceof Long ||
                    o instanceof Short ||
                    o instanceof String) {
                return o.toString();
            }
            if (o.getClass().getPackage().getName().startsWith("java.")) {
                return o.toString();
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    private static String wrap(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            if (obj instanceof Boolean ||
                    obj instanceof Byte ||
                    obj instanceof Character ||
                    obj instanceof Double ||
                    obj instanceof Float ||
                    obj instanceof Integer ||
                    obj instanceof Long ||
                    obj instanceof Short ||
                    obj instanceof String) {
                return (String) obj;
            }
            if (obj.getClass().getPackage().getName().startsWith("java.")) {
                return obj.toString();
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    void setDatainBundle() {
//        if (!action.equalsIgnoreCase(Constants.sign_up) && !action.equalsIgnoreCase(Constants.login)) {
//        }


    }

    public void postNewCommentretro() {

//        Syso.debug("url     :" + url + "    bundleData: " + bundleData);
//        RequestQueue queue = Volley.newRequestQueue(context);
//        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                dismissDialog();
//                Syso.debug("Site: " + url + "           " + response);
//
//                if (callBackListner != null)
//                    callBackListner.AsynctaskResult(response + "", action, bundleData, true);
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                dismissDialog();
//                Utils.showToastS(context.getString(R.string.something_went_wrong), context);
//                if (callBackListner != null)
//                    callBackListner.AsynctaskResult("", action, bundleData, false);
//                error.printStackTrace();
//            }
//        })
//
//        {
//            @Override
//            protected Map<String, String> getParams() {
//
//
//                Map<String, String> params1 = new HashMap<>();
//                params1.put("apikey", "ef131d95a49d60233c64d1629a3f2239");
//
//                for (Map.Entry<?, ?> entry : bundleData.entrySet()) {
//                    String key = (String) entry.getKey();
//                    if (key == null) {
//                        throw new NullPointerException("key == null");
//                    }
//                    try {
//                        params1.put(key, wrap(entry.getValue()));
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//
//                Syso.debug("data " + params1);
//                return params1;
//            }
//        };
//        sr.setRetryPolicy(new DefaultRetryPolicy(
//                10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        queue.add(sr);
//
//        RestAdapter adapter = builder.build();
//        RestInterface restInt = adapter.create(RestInterface.class);
//        restInt.getUser(bundleData, new Callback<Object>() {
//            @Override
//            public void success(Object s, Response response) {
//                dismissDialog();
//                Syso.debug("Site: " + url + "           " + s);
//                if (callBackListner != null) {
//                    Gson gson = new Gson();
//                    String json = gson.toJson(s);
//                    callBackListner.AsynctaskResult(json + "", action, bundleData, true);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissDialog();
//                error.printStackTrace();
//                Utils.showToastS(context.getString(R.string.something_went_wrong), context);
//                if (callBackListner != null)
//                    callBackListner.AsynctaskResult("", action, bundleData, false);
//            }
//        });
    }

    private void dismissDialog() {
        try {
            if (dialogShow)
                progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public interface AsynctaskListner {
        void AsynctaskResult(String arr, String action, HashMap<String, Object> par, boolean status);
    }

    class AsyncTaskApp1 extends AsyncTask<Bundle, Void, String> {

        @Override
        protected String doInBackground(Bundle... params1) {
            try {
//                Syso.debug(bundleData);
                url = url.replace(" ", "%20");
                ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                try {
                    if (bundleData != null) {
                        for (String key : bundleData.keySet()) {
                            Object value = bundleData.get(key);
                            if (value instanceof Integer) {
                                nameValuePair.add(new BasicNameValuePair(key,
                                        String.valueOf((Integer) value)));
                            } else if (value instanceof Long) {
                                nameValuePair.add(new BasicNameValuePair(key,
                                        String.valueOf((Long) value)));
                            } else if (value instanceof String) {
                                nameValuePair.add(new BasicNameValuePair(key,
                                        (String) value));
                            }
                        }
                    }
                } catch (Exception e) {
                }
                return SimpleHTTPConnection.sendByPOST(context, url,
                        nameValuePair);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            dismissDialog();

            if (callBackListner != null)
                callBackListner.AsynctaskResult(response + "", action, bundleData, true);
        }
    }
}