package com.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by vindhyachal on 13/4/17.
 */

public class MyfirebaseInstanceidservice extends FirebaseInstanceIdService {
    private static final String reg_token= "REG_TOKEN";

    @Override
    public void onTokenRefresh() {
        String recent_token= FirebaseInstanceId.getInstance().getToken();
        Log.d(reg_token, recent_token);
    }


}
