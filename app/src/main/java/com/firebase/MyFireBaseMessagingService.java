package com.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.villasfood.AudioNotifiaction;
import com.villasfood.BackUpActivityCheff;
import com.villasfood.MainActivity;
import com.villasfood.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import static com.villasfood.LoginActivity.checkCheff;

/**
 * Created by vindhyachal on 13/4/17.
 */

public class MyFireBaseMessagingService extends FirebaseMessagingService {
    Handler mHandler;
    Intent intent;

    public MyFireBaseMessagingService() throws ExecutionException, InterruptedException {

    }

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {

        try{


            JSONObject objectData=new JSONObject(remoteMessage.getData().get("message"));


            if(objectData.getString("is_user").equalsIgnoreCase("1")){
                checkCheff=false;
                intent= new Intent(this, MainActivity.class);
            }else{
                checkCheff=true;
                intent= new Intent(this, BackUpActivityCheff.class);
            }

            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent= PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notificationBuilder= new NotificationCompat.Builder(this);
            notificationBuilder.setContentTitle(objectData.getString("title"));
            notificationBuilder.setContentText(objectData.getString("body"));
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setLights(Color.BLUE,1,1);
            notificationBuilder.setSmallIcon(R.mipmap.appcon);
            notificationBuilder.setSound(defaultSoundUri);
            notificationBuilder.setContentIntent(pendingIntent);
            NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notificationBuilder.build());


            PowerManager pm = (PowerManager)getApplicationContext().getSystemService(Context.POWER_SERVICE);
            boolean isScreenOn = pm.isScreenOn();
            if(isScreenOn==false)
            {
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
                wl.acquire(10000);
                PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
                wl_cpu.acquire(10000);
            }

            Intent iRef=new Intent(this, AudioNotifiaction.class);
            iRef.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            iRef.putExtra("message",objectData.getString("body"));
            startActivity(iRef);


        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    

}
